/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _DICTIONARY_H_
#define _DICTIONARY_H_

#include <stdint.h> // for int32_t

//! @file
//! @brief Header for dictionary.c

//! Typedef for \a struct \a _Dictionary
typedef struct _Dictionary Dictionary; // do not expose struct

//! Typedef for \a struct \a _DictChar
typedef struct _DictChar DictChar;

Dictionary * mallocDictionary(unsigned int capacity);
int freeDictionary(Dictionary ** d);

int32_t getCodeAt(const Dictionary * d, int index);
int32_t getCharAt(const Dictionary * d, int index);
unsigned int getLength(const Dictionary * d);
unsigned int getCapacity(const Dictionary * d);

int32_t getLetterByBinary(Dictionary * d, const int32_t code);
int32_t getBinaryByLetter(Dictionary * d, const int32_t letter);

int setCodeAt(Dictionary * d, const int index, const int32_t value);
int setCharAt(Dictionary * d, const int index, const int32_t value);
int addCodeChar(Dictionary * d, const int32_t character, const int32_t code);

int cleanDictionary(Dictionary *d);

char * mallocBinaryToDitDot(int32_t code, int * resSize);
int32_t ditdotToBinary(const char * code);
int isMorseCoded(const char * pi);

//DictChar toDictChar(int32_t dictionaryChar);
DictChar * mallocDictChar();
int fillDictChar(DictChar * dc, int32_t dictionaryChar);
char * getDictCharPtr(DictChar *dc);

#endif
