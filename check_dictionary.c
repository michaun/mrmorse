/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <check.h>

#include "dictionary.h"

START_TEST(testMallocDictionary)
{
  Dictionary * d = mallocDictionary(40);
  ck_assert_ptr_nonnull(d);
  ck_assert_int_eq(40, getCapacity(d));
  ck_assert_int_eq(0, getLength(d));
  freeDictionary(&d);
} END_TEST

START_TEST(testAddChar)
{
  Dictionary * d = mallocDictionary(40);
  addCodeChar(d, 'A', 0b110);
  addCodeChar(d, 'B', 0b11000);
  addCodeChar(d, 'C', 0b11010);
  //
  ck_assert_int_eq(0b110,   getCodeAt(d, 0));
  ck_assert_int_eq(0b11000, getCodeAt(d, 1));
  ck_assert_int_eq(0b11010, getCodeAt(d, 2));
  ck_assert_int_eq('A', getCharAt(d, 0));
  ck_assert_int_eq('B', getCharAt(d, 1));
  ck_assert_int_eq('C', getCharAt(d, 2));
  ck_assert_int_eq('A', getLetterByBinary(d, 0b110));
  ck_assert_int_eq('B', getLetterByBinary(d, 0b11000));
  ck_assert_int_eq('C', getLetterByBinary(d, 0b11010));
  ck_assert_int_eq(0b110,   getBinaryByLetter(d, 'A'));
  ck_assert_int_eq(0b11000, getBinaryByLetter(d, 'B'));
  ck_assert_int_eq(0b11010, getBinaryByLetter(d, 'C'));
  //
  freeDictionary(&d);
} END_TEST
    
START_TEST(testSet)
{
  Dictionary * d = mallocDictionary(40);
  addCodeChar(d, 'A', 0b110);
  addCodeChar(d, 'B', 0b11000);
  addCodeChar(d, 'C', 0b11010);
  //
  setCodeAt(d, 0, 0b1100);
  ck_assert_int_eq(0b1100, getBinaryByLetter(d, 'A'));
  setCharAt(d, 0, 'D');
  ck_assert_int_eq('D', getLetterByBinary(d, 0b1100));

  setCodeAt(d, 0, 0b110);
  setCharAt(d, 0, 'A');
  ck_assert_int_eq(0b110, getBinaryByLetter(d, 'A'));
  //
  freeDictionary(&d);
} END_TEST

START_TEST(testUnusual)
{
  Dictionary * d = mallocDictionary(40);
  addCodeChar(d, 'A', 0b110);
  addCodeChar(d, 'B', 0b11000);
  addCodeChar(d, 'C', 0b11010);
  //
  ck_assert_int_eq(-1, getBinaryByLetter(d, 'E'));
  ck_assert_int_eq(-1, getLetterByBinary(d, 0b100000));
  ck_assert_int_eq(-1, getLetterByBinary(d, 0));
  ck_assert_int_eq(-1, getCharAt(d, 5));
  ck_assert_int_eq(-1, getCodeAt(d, 5));
  ck_assert_int_eq(-1, getCharAt(d, 500));
  ck_assert_int_eq(-1, getCodeAt(d, 500));
  //
  freeDictionary(&d);
} END_TEST

START_TEST(testClean)
{
  Dictionary * d = mallocDictionary(40);
  addCodeChar(d, 'A', 0b110);
  addCodeChar(d, 'B', 0b11000);
  addCodeChar(d, 'C', 0b11010);
  //
  cleanDictionary(d);
  ck_assert_int_eq(-1, getBinaryByLetter(d, 'A'));
  ck_assert_int_eq(-1, getBinaryByLetter(d, 0b110));
  ck_assert_int_eq(-1, getCharAt(d, 0));
  ck_assert_int_eq(-1, getCodeAt(d, 0));
  //
  freeDictionary(&d);
} END_TEST

START_TEST(testFreeDictionary)
{
  Dictionary * d = mallocDictionary(40);
  freeDictionary(&d);
  ck_assert_ptr_null(d);
} END_TEST


Suite* DictionaryTestSuite() {
  Suite* suite;
  TCase *tccore;
  suite = suite_create("Dictionary Tests");
  tccore = tcase_create("Core");
  
  tcase_add_test(tccore, testMallocDictionary);  
  tcase_add_test(tccore, testAddChar);
  tcase_add_test(tccore, testSet);
  tcase_add_test(tccore, testUnusual);
  tcase_add_test(tccore, testClean);
  tcase_add_test(tccore, testFreeDictionary);
  suite_add_tcase(suite, tccore);

  return suite;
}

