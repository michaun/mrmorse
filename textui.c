/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "textui.h"
#include "soundconv.h"
#include "textconv.h"
#include "parser.h"
#include "dictionary.h"
#include "arghandler.h"
#include "toolkit.h"
#include "version.h"


//! @file
//! @brief Contains functions fo using text UI (cmdline UI)

//! @brief print string
//!
//! @param[in] out string to print to \a stdout
//! @return value from printf
//! @note: this function is static

static int showString(char * out)
{
  return printf("%s", out);
}

//! @brief start command-line UI
//!
//! Manages so-called text UI. This is the command line interface.
//! This is the default interface for MrMorse.
//! It acts as a batch script or ad-hoc commandline translator.
//! @param[in] *args pointer to an argStruct structure (parsed command line args)

void textUIStart(argStruct * args)
{

  Dictionary * dict = mallocDictionary(40);

  // parse file into arrays
  loadDictionaryFromFile(getArgDictfile(args), dict);

  /// testarea
  char * outbuffer = NULL;
  char * inbuffer = NULL;
  
  FILE * fp = NULL;
  if(!getArgInputfile(args)) {
    showString("Welcome to "NAME" "VERSION"!\n\n\
Please type in your message\n\
For Morse Code to Text:\n\
- use '.' for a dot\n\
- use '-' for a dash\n\
- use ' ' to separate character codes\n\
- use ' | ' (note spaces!) to separate words\n\
Press CTRL-D to translate and finish\n\n");
    fp = stdin;
  } else {
    if (NULL == (fp = fopen(getArgInputfile(args), "r"))) {
      return;
    }
  }

  inbuffer = mallocLoadBufferFromFileHandle(fp);

  switch(getArgOutputtype(args)) {
    //    case STDOUT:
  case ARGTEXT:
    if(*inbuffer != '\n')
      outbuffer = mallocConvertTxt(dict, inbuffer, NULL);
    break;
  case ARGAUDIO:
    convertSnd(dict, inbuffer, getArgOutputfile(args));
    break;
  }
  
  if(getArgOutputtype(args) == ARGTEXT) {
    if(!getArgOutputfile(args)) {
      showString("\n\n=== Translation Begin ===\n");
      showString(outbuffer);
      showString("\n=== Translation End ===\n");
    } else {
      saveBufferToFile(outbuffer, getArgOutputfile(args));
    }
  }
  
  if(inbuffer)  freeNullify((void**)&inbuffer);
  if(outbuffer) freeNullify((void**)&outbuffer);
  // cleanup and exit
  freeDictionary(&dict);

  if(fp != stdin)
    fclose(fp);
  /// end testarea
}
