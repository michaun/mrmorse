/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#include "arghandler.h"
#include "version.h"

#ifdef HAS_GTK3
#include "gtkgui.h"
#endif

#ifdef HAS_NCURSES
#include "ncursesui.h"
#endif

/*!
\mainpage MrMorse Documentation

\section intro_sec Introduction

MrMorse is supposed to be a helper app for those who intend to learn morse code.

\section prereq_sec Prerequirement
MrMorse requires:
- libao (https://xiph.org/ao/)
- libsndfile (http://www.mega-nerd.com/libsndfile/)

To run tests you need libcheck (https://libcheck.github.io/check/)

To build MrMorse requires CMake >= 3.7 (https://cmake.org/)

For specific user interfaces MrMorse requires (optionally):
- GTK3 for GTK interface (https://www.gtk.org/)
- NCurses along with libmenu and libform for NCurses interface (https://www.gnu.org/software/ncurses/ncurses.html)

\section install_sec Installation

~~~
$ git clone https://gitlab.com/michaun/mrmorse
$ cd mrmorse
$ cmake CMakeLists.txt
~~~

To build all (prog + tests)
~~~
$ make
~~~

To build the program
~~~
$ make mrmorse
~~~

To build tests
~~~
$ make checkall
~~~

To run unit tests on the software you can do:
~~~
$ ./checkall
~~~

To install (as superuser):
~~~
$ make install
~~~

By default program installs as mrmorse and has a mrmorse (1) man page.

\section usage_sec Usage
Please refer to manual page and/or help screen (-h) for details.

\section uninstall_sec Uninstallation
MrMorse installs only one binary and a manual page.
CMake creates an installation manifest (install_manifest.txt) that lists all installed files.
To utilise this file one may do:
~~~
# xargs rm < install__manifest.txt
~~~
which will remove all installed files.

\section Copyright
For license please refer to COPYING file.

\section Kudos
Kudos for these helpful libao and libsndfile examples\n
https://xiph.org/ao/doc/ao_example.c\n
https://gist.github.com/maxammann/52d6b65b42d8ce23512a\n

*/

// forward declaration
void textUIStart(argStruct * args);

// static
static void printStr(char * str);
static void printHelp(char * programname);

int main(int argc, char *argv[])
{
  argStruct * args = mallocArgStruct();
  parseArgs(argc, argv, args);

  if(getArgHelp(args)) {
    printHelp(argv[0]);
    return 0;
  }

  if(getArgVersion(args)) {
    printStr(VERSION"\n");
    return 0;
  }

  // check if we received dictionary
  if(!getArgDictfile(args)) {
    printHelp(argv[0]);
    return 1;
  }

  // run GUI or TUI
  if(getArgGUIOn(args)) {
#ifdef HAS_GTK3
    createGtkWindow(args);
#else
    printStr("GTK3 support is not compiled\nCannot run in GTK3 mode\n");
#endif
  } else if(getArgNCOn(args)) {
#ifdef HAS_NCURSES
    ncursesUIStart(args);
#else
    printStr("NCurses support is not compiled\nCannot run in NCurses mode\n");
#endif
  } else {
    textUIStart(args);
  }

  freeArgStruct(&args);
  // return
  return 0;
}

//! @brief print string
//!
//! @param[in] str string to print out to \a stdout
//! @note: this function is static

static void printStr(char * str)
{
  fprintf(stdout, "%s", str);
}

//! @brief print help message
//!
//! @param[in] programname Name of the program
//! @note: this function is static

static void printHelp(char * programname)
{
  fprintf(stderr, "%s\n\nUsage: %s -d dictfile [-i inputfile] [-o outputfile] [-t newtype] [-g] [-n] [-v]\n\n-h this help screen\n-d dictionary file\n-i input file\n-o outputfile (matters only in cmdline mode)\n-t change outputtype (matters only in cmdline mode)\n-g use GUI (GTK3, if available)\n-n use NCurses (if available)\n-v Print version and exit\n\n",DESCRIPTION, programname);
}
