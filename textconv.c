/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "textconv.h"
#include "dictionary.h"
#include "toolkit.h"

//! @file
//! @brief Functions for text conversion

static void convertTxtToken(Dictionary * dict, const char * input, char ** outbuffer, int bufSize, char isMorseCoded)
{
  char * tempbuf;
  int32_t c;
  int byteCount;
  DictChar * dc;
  if(input == NULL) return;
  if(strlen(input) == 0) return;
  dc = mallocDictChar();
  if(isMorseCoded) {
    if(strlen(*outbuffer) > bufSize-1) { freeNullify((void**)&dc); return; }
    if(*input == '|') {
      strncpy(*outbuffer + strlen(*outbuffer), " ", 1);
    } else if(*input == '?') {
      // do absolutely nothing
    } else {
      fillDictChar(dc, getLetterByBinary(dict, ditdotToBinary(input)));
      strncpy(*outbuffer + strlen(*outbuffer), getDictCharPtr(dc), strlen(getDictCharPtr(dc)));
    }
  } else {
    while(*input != '\0') {
      if(*input == '\n') {
        strncpy(tempbuf, "|\n\0", 3);
      } else {
        c = readNextUTF8Buf((unsigned char *)input, &byteCount);
        if(c >= 'A' && c <= 'Z') c += 0x20;
        tempbuf = mallocBinaryToDitDot(getBinaryByLetter(dict, c), NULL);
        tempbuf[strlen(tempbuf)] = ' ';
      }
      if(strlen(*outbuffer) + strlen(tempbuf) > bufSize-1) { freeNullify((void**)&tempbuf); break; }
      strncpy(&(*outbuffer)[strlen(*outbuffer)], tempbuf, strlen(tempbuf));
      input+=byteCount;
      freeNullify((void**)&tempbuf);
    }
    // we done. after last char there is no need for space
    if(strlen(*outbuffer) > 0 &&
       strlen(*outbuffer) <= bufSize &&
       (*outbuffer)[strlen(*outbuffer)-1] == ' ') {
      (*outbuffer)[strlen(*outbuffer)-1] = '\0';
    }
  }
  (*outbuffer)[bufSize-1] = '\0';
  freeNullify((void**)&dc);
}

//! @warning only for tests, must not be used in the project
void test_convertTxtToken(Dictionary * dict, const char * input, char ** outbuffer, int bufSize, char isMorseCoded)
{
  convertTxtToken(dict, input, outbuffer, bufSize, isMorseCoded);
}

static void convertTxtLine(Dictionary * dict, const char * input, char ** outbuffer, int bufSize)
{
  char *workingbuffBeginPtr = NULL;
  if(NULL == input) return;
  if(0 == strlen(input)) return;

  char isMCoded = isMorseCoded(input);
  char * workingbuff = calloc(strlen(input)+1, sizeof(char));
  if(!workingbuff) return;
  workingbuffBeginPtr = workingbuff;
  strncpy(workingbuff, input, strlen(input));

  char * pch = strsep(&workingbuff, " ");
  while(pch != NULL) {
    convertTxtToken(dict, pch, outbuffer, bufSize, isMCoded);
    pch = strsep(&workingbuff, " ");
    if(!isMCoded && pch && strlen(pch)) // there is another token, separate it with '/'
                                        // do it only when translating to Morse Coded
      strncpy(&(*outbuffer)[strlen(*outbuffer)], " | ", 3);
  }
  freeNullify((void**)&workingbuffBeginPtr);
}

//! @warning only for tests, must not be used in the project
void test_convertTxtLine(Dictionary * dict, const char * input, char ** outbuffer, int bufSize)
{
  convertTxtLine(dict, input, outbuffer, bufSize);
}

//! @brief Using \a dict the function converts \a input into translated text output buffer, if \a bufSize passed, fills it with buffer size
//!
//! @param[in] dict Dictionary that provides translation
//! @param[in] input buffer to translate
//! @param[out] bufSize is filled with output buffer size (if argument not NULL)
//! @retval char* allocated translated buffer
//! @retval NULL if something went wrong
//! @warning The reurned buffer has to be freed

char * mallocConvertTxt(Dictionary * dict, const char * input, int * bufSize)
{
  char * tempbuf = NULL;
  char * outbuffer = NULL;
  char *workingbuffBeginPtr = NULL;
  int tempbufSize = 0;
  int tempbufLen = 0;
  int inputLen = strlen(input);
  
  if(NULL == input) return NULL;
  if(0 == inputLen) return NULL;

  char * workingbuff = calloc(inputLen+1, sizeof(char));
  if(!workingbuff) return NULL;
  workingbuffBeginPtr = workingbuff;
  strncpy(workingbuff, input, inputLen);

  tempbufSize = (8 * sizeof(int32_t) * inputLen) + 1;
  tempbuf = calloc(tempbufSize, sizeof(char));
  if(!tempbuf) { freeNullify((void**)&workingbuff); return NULL; }

  memset(tempbuf, 0, tempbufSize); // no trust

  char * pch = strsep(&workingbuff, "\n");
  while(pch != NULL) {
    convertTxtLine(dict, pch, &tempbuf, tempbufSize);
    pch = strsep(&workingbuff, "\n");
    if(pch)
      strncpy(tempbuf+strlen(tempbuf), "\n", 1);
  }
  tempbufLen = strlen(tempbuf);
  tempbuf[tempbufLen] = '\0';
  outbuffer = calloc(tempbufLen + 1, sizeof(char));
  if(!outbuffer) { freeNullify((void**)&workingbuffBeginPtr); freeNullify((void**)&tempbuf); return NULL; }
  strncpy(outbuffer, tempbuf, tempbufLen + 1);
  if(bufSize)
    *bufSize = tempbufLen+1;
//  printf("%s - %lu - %u - %lu\n\n", outbuffer, strlen(outbuffer), strlenUTF8((unsigned char *)outbuffer), strlen(tempbuf)+1);

  freeNullify((void**)&workingbuffBeginPtr);
  freeNullify((void**)&tempbuf);
  return outbuffer;
}
