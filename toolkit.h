/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _TOOLKIT_H_
#define _TOOLKIT_H_

#include <stdio.h>

//! @file
//! @brief Header for toolkit.c

int saveBufferToFile(const char * buffer, const char * filename);
char * mallocLoadBufferFromFile(const char * filename);
char * mallocLoadBufferFromFileHandle(FILE * fp);
int32_t readNextUTF8(FILE * fp, int * countBytes);
int32_t readNextUTF8Buf(const unsigned char * buffer, int * countBytes);
int firstCharBytesUTF8(const unsigned char * buffer);
int strlenUTF8(const unsigned char * buffer);
void freeNullify(void ** ptr);

#endif
