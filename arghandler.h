/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _ARGHANDLER_H_
#define _ARGHANDLER_H_

//! @file
//! @brief Header for arghandler.c

//! @brief Output type enum
enum {
  ARGTEXT,  //!< Output type is text
  ARGAUDIO, //!< Output type is audio
  };

//! Typedef for a \a struct \a _argStruct
typedef struct _argStruct argStruct;

//alloc/free
argStruct * const mallocArgStruct();
void freeArgStruct(argStruct ** args);

// getters
const char * getArgDictfile(const argStruct * const args);
const char * getArgInputfile(const argStruct * const args);
const char * getArgOutputfile(const argStruct * const args);
const char getArgOutputtype(const argStruct * const args);
const char getArgGUIOn(const argStruct * const args);
const char getArgNCOn(const argStruct * const args);
const char getArgVersion(const argStruct * const args);
const char getArgHelp(const argStruct * const args);

void parseArgs(int argc, char *argv[], argStruct * const args);

#endif
