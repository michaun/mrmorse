/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _VERSION_H_
#define _VERSION_H_

//! @file
//! @brief This file contains version-related strings

//! Program's version
#define VERSION "0.0.0rc1"

//! Programs description
#define DESCRIPTION "MrMorse is a text-to-morse and back translator"

//! Program's name
#define NAME "MrMorse"

//! Program's copyright information
#define COPYRIGHT "Copyright (C) 2017 - 2018 Michal Niezborala"

#endif
