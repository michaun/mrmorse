/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>

#include "toolkit.h"
#include "dictionary.h"

//! @file
//! @brief Contains functions to handle Dictionary structure

//! @brief Contains character in a more UTF8-friendly form
//! This structure contains an array of 5 to allocate up-to-4byte UTF8 character code and a NULL character
//! Because UTF8 is a variable-length encoding, the structure also contains a pointer to the beginning of
//! the actual starting byte in the array

struct _DictChar {
  char b[5];  //!< byte array for up-to-4byte UTF8 code + '\0'
  char *bptr; //!< pointer to first meaningful byte (eg. for 2byte UTF8
              //!< code this would point to b[2], the array is then
              //!< b[0] = 0, b[1] = 0, b[2] = first byte, b[3] = second byte, b[4]='\0')
};

//! @brief Contains character and code information
//!
//! Dictionary structure contains two pointers to arrays
//! allocated on creation, one of the arrays (\a chararray)
//! contains characters, the other one contains their codes
//! (\a codearray). Indicies of both arrays have to match.
//! \a arraylen tells how many entries are there (in terms
//! of count) and \a capacity tells how much space do we have
//! in the dictionary.

struct _Dictionary {
  int32_t * chararray; //!< pointer to a character array
  int32_t * codearray; //!< pointer to a Morse code array
  unsigned int arraylen; //!< length of arrays (as in number of elements)
  unsigned int capacity; //!< capacity of arrays (as in size of the array)
};

//! @brief create a \a Dictionary of specified \a capacity
//!
//! @param[in] capacity of a \a Dictionary
//! @retval Pointer to a \a Dictionary just created
//! @retval NULL if something went wrong
//! @warning This function allocates memory, use freeDictionary to free that pointer

Dictionary * mallocDictionary(unsigned int capacity)
{
  Dictionary * d = calloc(1, sizeof(Dictionary));
  if(!d) return NULL;
  // create arrays
  d->capacity = capacity;
  d->chararray = (int32_t*) calloc(capacity, sizeof(int32_t));
  if(!d->chararray) { freeNullify((void**)&d); return NULL; }
  d->codearray = (int32_t*) calloc(capacity, sizeof(int32_t));
  if(!d->codearray) { freeNullify((void**)&d->chararray); freeNullify((void**)&d); return NULL; }
  d->arraylen = 0;
  return d;
}

//! @brief completely free a \a Dictionary
//!
//! Free both internal arrays of a Dictionary,
//! Dictionary itself, set the pointer to NULL.
//! @param[in] d Dictionary to be freed.
//! @retval -1 if the \a Dictionary pointer is NULL
//! @retval 0 on success

int freeDictionary(Dictionary ** d)
{
  if(!d && !(*d)) return -1;
  freeNullify((void**)&(*d)->chararray);
  freeNullify((void**)&(*d)->codearray);
  freeNullify((void**)&(*d));
  *d = NULL;
  return 0;
}

//! @brief get Code from \a Dictionary's code array (internal array)
//!
//! @param[in] d Dictionary from which to pick the code.
//! @param[in] index of an array.
//! @retval char representing Morse Code, see \a _Dictionary structure for more info on how it is stored  
//! @retval -1 if index is out of bounds

int32_t getCodeAt(const Dictionary * d, int index)
{
  if(d->arraylen > index)
    return d->codearray[index];
  else
    return -1;
}

//! @brief get character from \a Dictionary's character array (internal array)
//!
//! @param[in] d Dictionary from which to pick the character.
//! @param[in] index of an array.
//! @return Character or -1 if index is out of bounds

int32_t getCharAt(const Dictionary * d, int index)
{
  if(d->arraylen > index)
    return d->chararray[index];
  else
    return -1;
}

//! @brief get length of the Dictionary (number of non-NULL entries)
//!
//! @param[in] d Dictionary
//! @return Number of non-NULL entries in Dictionary

unsigned int getLength(const Dictionary * d)
{
  return d->arraylen;
}

//! @brief get capacity of a \a Dictionary (space it has)
//!
//! @param[in] d Dictionary
//! @return Number of entries Dictionary can hold

unsigned int getCapacity(const Dictionary * d)
{
  return d->capacity;
}

//! @brief get character by searching with \a code
//!
//! @param[in] code a code used to find a character
//! @param[in, out] d Dictionary
//! @return a character from Dictionary associated with \a code
//! @return -1 if not found

int32_t getLetterByBinary(Dictionary * d, const int32_t code)
{
  for(int i = 0; i < getLength(d); i++) {
    if(getCodeAt(d, i) == code) {
      return getCharAt(d, i);
    }
  }
  return -1;
}

//! @brief get a code by searching with \a letter (character)
//!
//! @param[in] letter a character used to find a code
//! @param[in, out] d Dictionary
//! @return a code from Dictionary associated with a \a letter (character)
//! @return -1 if not found

int32_t getBinaryByLetter(Dictionary * d, const int32_t letter)
{
  for(int i = 0; i < getLength(d); i++) {
    if(getCharAt(d, i) == letter) {
      return getCodeAt(d, i);
    }
  }
  return -1;
}

//! @brief set Code in \a Dictionary's code array (internal array) at given \a index
//!
//! @param[in, out] d Dictionary in which to set the code.
//! @param[in] index of an array.
//! @param[in] value value of code to set.
//! @retval 0, on success
//! @retval -1 if index is out of bounds

int setCodeAt(Dictionary * d, const int index, const int32_t value)
{
  if(index >= getCapacity(d)) return -1;
  d->codearray[index] = value;
  return 0;
}

//! @brief set character in \a Dictionary's character array (internal array) at given \a index
//!
//! @param[in, out] d Dictionary in which to set the code.
//! @param[in] index of an array.
//! @param[in] value value of a character to set.
//! @retval 0, on success
//! @retval -1 if index is out of bounds

int setCharAt(Dictionary * d, const int index, const int32_t value)
{
  if(index >= getCapacity(d)) return -1;
  d->chararray[index] = value;
  return 0;
}

//! @brief add \a character with its \a code into the \a d Dictionary
//!
//! @param[in, out] d Dictionary to which character and code are added.
//! @param[in] character to add.
//! @param[in] code to add.
//! @retval 0, on success
//! @retval -1 if reallocation (happens when dictionary is full) fails

int addCodeChar(Dictionary * d, const int32_t character, const int32_t code)
{
  if(getLength(d) == getCapacity(d)) {
    d->capacity *= 2;
    if(NULL == (d->codearray = realloc(d->codearray, d->capacity * sizeof(int32_t)))) return -1;
    if(NULL == (d->chararray = realloc(d->chararray, d->capacity * sizeof(int32_t)))) return -1;
  }
  setCodeAt(d, getLength(d), code);
  setCharAt(d, getLength(d), character);
  d->arraylen++;
  return 0;
}

//! @brief Set entries of \a Dictionary to 0 and set its length to zero
//!
//! @param[in, out] d Dictionary to clean
//! @retval 0, on success

int cleanDictionary(Dictionary *d)
{
  int len = getLength(d);
  for(int i = 0; i < len; i++) {
    setCodeAt(d, i, 0);
    setCharAt(d, i, 0);
  }
  d->arraylen = 0;
  return 0;
}

//! @brief Allocate and return a string consisting of "." and "-" for code, optionally return size of this string
//!
//! @param[in] code integer value that is to be translated to dit-dot string
//! @param[out] resSize optional argument, if resSize is a pointer, that pointer is filled with allocated buffer size
//! @retval char* allocated dit-dot string
//! @retval NULL if string failed to allocate
//! @warning The returned buffer has to be freed

char * mallocBinaryToDitDot(int32_t code, int * resSize)
{
  int i;
  unsigned int bufSize = 8;
  char * buffer = calloc(bufSize, sizeof(char));
  if(!buffer) return NULL;
  char * result = calloc(bufSize, sizeof(char));
  if(!result) { freeNullify((void**)&buffer); return NULL; }
  if(resSize)
    *resSize = bufSize;

  if(code == -1) { strncpy(result, "?", 1); return result; }
  
  for(i = 0; code != 1 && code != -1; i++) {
    if(i == bufSize) {
      bufSize *= 2;
      buffer = realloc(buffer, bufSize * sizeof(char));
      if(!buffer) return NULL;
      result = realloc(result, bufSize * sizeof(char));
      if(!result) { freeNullify((void**)&buffer); return NULL; }
    }
    if(code & 1) buffer[i] = '-';
    else buffer[i] = '.';
    code >>= 1;
  }
  buffer[i] = 0;

  for(i = 0; i < strlen(buffer); i++) {
    result[i] = buffer[strlen(buffer) - 1 - i];
  }
  result[i] = 0;

  freeNullify((void**)&buffer);
  return result;
}

//! @brief Convert ditdot string \a code into an integer representation
//!
//! @param[in] code string that is to be translated into integer
//! @retval int32_t integer that represents ditdot code
//! @retval NULL if string failed to allocate or code is NULL

int32_t ditdotToBinary(const char * code)
{
  int32_t ret = 1; // to differentiate between .- (01) and - (1) we preceed whole thing with a one that is not part of he code itself
  // that way we can distinguish 101 => 01 => .- => A and 11 => 1 => - => T

  if (code == NULL) return -1;
  
  for(int i = 0; code[i] != '\0'; i++) {
    if(code[i] == '.') {
      ret <<= 1;
    } else if(code[i] == '-') {
      ret <<= 1;
      ret |= 1;
    } else {
      return -1; // not a ditdot string!
    }
  }
  return ret;
}

//! @brief Checks if the string is Morse encoded
//!
//! @param[in] pi string to test
//! @retval -1 if string \a pi is NULL
//! @retval 0 if string \a pi is normal text
//! @retval 1 if string \a pi is Morse-coded text

int isMorseCoded(const char * pi)
{
  if(!pi) return -1;
  
  while(*pi != '\0') {
    if(*pi != '.' && *pi != '-' && *pi != '?' &&
       *pi != ' ' && *pi != '|' && *pi != '\n') return 0;
    pi++;
  }
  return 1;
}

//! @brief Creates a \a DictChar structure
//!
//! @retval DictChar* pointer to a DictChar structure
//! @retval NULL if allocation failed

DictChar * mallocDictChar()
{
  DictChar * dc = calloc(1, sizeof(DictChar));
  return dc;
}

//! @brief Fills \a DictChar structure with 4-byte int representation
//!
//! Purpose of this function is to prevent any endianess mishaps.
//! It creates an ordered char array with a pointer to the beginning of it
//! @param[out] *dc pointer to \a DictChar structure to fill
//! @param[in] dictionaryChar integer character to put into DictChar array
//! @retval -1 if \a DictChar structure to fill is NULL
//! @retval 0 on success

int fillDictChar(DictChar * dc, int32_t dictionaryChar)
{
  int i;
  if(!dc) return -1;
  memset(dc, 0, sizeof(DictChar));
  for(i = 3; i >= 0; i--) {
    dc->b[i] = dictionaryChar & 0xFF; dictionaryChar >>= 8;
  }
  dc->b[4] = '\0';
  dc->bptr = dc->b;
  for(i = 0; i < 4; i++, dc->bptr++) {
    if(*dc->bptr) break;
  }
  return 0;
}

//! @brief Get pointer to character array
//!
//! @param[in] dc DictChar from which to get the pointer
//! @retval char* pointer to the beginning of character in the character array

char * getDictCharPtr(DictChar * dc)
{
  return dc->bptr;
}
