/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <check.h>

#include "dictionary.h"
#include "parser.h"

START_TEST(testLoadDictFromEmptyFileName)
{
  Dictionary * d = mallocDictionary(40);
  char * input = "";
  loadDictionaryFromFile(input, d);
  int actual = getLength(d);
  int expected = 0;
  ck_assert_int_eq(expected, actual);
  freeDictionary(&d);
} END_TEST

START_TEST(testLoadDictFromDictTxtFileName)
{
  Dictionary * d = mallocDictionary(40);
  char * input = "dict.txt";
  loadDictionaryFromFile(input, d);
  int actual = getLength(d);
  int expected = 41;
  ck_assert_int_eq(expected, actual);
  freeDictionary(&d);
} END_TEST

START_TEST(testLoadDictFromNonExistentDictTxtFileName)
{
  Dictionary * d = mallocDictionary(40);
  char * input = "&%$&&*dict.txt";
  loadDictionaryFromFile(input, d);
  int actual = getLength(d);
  int expected = 0;
  ck_assert_int_eq(expected, actual);
  freeDictionary(&d);
} END_TEST

START_TEST(testLoadDictIntoTooSmallDict)
{
  Dictionary * d = mallocDictionary(1);
  char * input = "dict.txt";
  loadDictionaryFromFile(input, d);
  int actual = getLength(d);
  int expected = 41;
  ck_assert_int_eq(expected, actual);
  freeDictionary(&d);
} END_TEST

START_TEST(testLoadDictWhenDictIsNULL)
{
  Dictionary * d = NULL;
  char * input = "dict.txt";
  loadDictionaryFromFile(input, d);
  Dictionary * actual = d;
  Dictionary * expected = NULL;
  ck_assert_ptr_eq(expected, actual);
} END_TEST

START_TEST(testLoadDictFileNameIsNULL)
{
  Dictionary * d = mallocDictionary(40);
  char * input = NULL;
  loadDictionaryFromFile(input, d);
  int actual = getLength(d);
  int expected = 0;
  ck_assert_int_eq(expected, actual);
  freeDictionary(&d);
} END_TEST


START_TEST(testIsMorseCoded1)
{
  ck_assert_int_eq(1, isMorseCoded(".-"));
} END_TEST

START_TEST(testIsMorseCoded2)
{
  ck_assert_int_eq(0, isMorseCoded(".-ALA"));
} END_TEST

START_TEST(testIsMorseCoded3)
{
  ck_assert_int_eq(0, isMorseCoded("A"));
} END_TEST

START_TEST(testIsMorseCoded4)
{
  ck_assert_int_eq(-1, isMorseCoded(NULL));
} END_TEST

START_TEST(testDitDotToBinary1)
{
  ck_assert_int_eq(0b101, ditdotToBinary(".-"));
} END_TEST

START_TEST(testDitDotToBinary2)
{
  ck_assert_int_eq(-1, ditdotToBinary("A"));
} END_TEST

START_TEST(testDitDotToBinary3)
{
  ck_assert_int_eq(-1, ditdotToBinary(NULL));
} END_TEST

Suite* ParserTestSuite() {
  Suite* suite;
  TCase *tcload, *tcismcode, *tcdtb;
  suite = suite_create("Parser Tests");
  //
  tcload = tcase_create("Load Dictionary");
  tcase_add_test(tcload, testLoadDictFromEmptyFileName);
  tcase_add_test(tcload, testLoadDictFromDictTxtFileName);
  tcase_add_test(tcload, testLoadDictFromNonExistentDictTxtFileName);
  tcase_add_test(tcload, testLoadDictIntoTooSmallDict);
  tcase_add_test(tcload, testLoadDictWhenDictIsNULL);
  tcase_add_test(tcload, testLoadDictFileNameIsNULL);
  suite_add_tcase(suite, tcload);
  //
  tcismcode = tcase_create("Is Morse Code");
  tcase_add_test(tcismcode, testIsMorseCoded1);
  tcase_add_test(tcismcode, testIsMorseCoded2);
  tcase_add_test(tcismcode, testIsMorseCoded3);
  tcase_add_test(tcismcode, testIsMorseCoded4);
  suite_add_tcase(suite, tcismcode);
  //
  tcdtb = tcase_create("DitDotToBinary");
  tcase_add_test(tcdtb, testDitDotToBinary1);
  tcase_add_test(tcdtb, testDitDotToBinary2);
  tcase_add_test(tcdtb, testDitDotToBinary3);
  suite_add_tcase(suite, tcdtb);

  return suite;
}
