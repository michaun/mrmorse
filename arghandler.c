/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "toolkit.h"
#include "arghandler.h"

//! @file
//! @brief Contains functions for \a argStruct structure handling.

//! @brief Contains parsed commandline arguments
struct _argStruct {
  //! Pointer to dictionary filename
  const char * dictfile; //!< This argument points to a filename
                         //!< of a dictionary file. This file must be
                         //!< "parseable" by the parser.

  //! Pointer to input file filename
  const char * inputfile; //!< This argument...
                          //!< ...
                          //!< ...
  const char * outputfile; //!< Pointer to output file filename
  char outputtype; //!< An output file type
  char GUIOn; //!< If picked, run GTK GUI
  char NCOn; //!< If picked, run NCurses UI
  char showVersion; //!< If picked, show version and exit
  char help; //!< If picked, print help and exit
};

//! @brief Allocates an argStruct structure
//!
//! @retval argStruct* pointer to an allocated argStruct structure
//! @retval NULL if structure couldn't be allocated
//! @warning argStruct has to be freed with freeArgStruct()

argStruct * const mallocArgStruct()
{
  argStruct * const args = calloc(1, sizeof(argStruct));
  if(!args) return NULL;
  return args;
}

//! @brief Frees allocated argStruct structure
//!
//! @param[in] *args pointer to an argStruct structure
//! @retval *argStruct pointer to allocated argStruct structure
//! @retval NULL if structure couldn't be allocated

void freeArgStruct(argStruct ** args)
{
  if(!args && !(*args)) return;
  freeNullify((void**)args);
  *args = NULL;
}

//! @brief Return \a dictfile member from \a argStruct structure
//!
//! @param[in] *args pointer to an argStruct structure
//! @retval char* \a dictfile pointer from \a argStruct \a args structure

const char * getArgDictfile(const argStruct * const args)
{
  return args->dictfile;
}

//! @brief Return \a inputfile member from \a argStruct structure
//!
//! @param[in] *args pointer to an argStruct structure
//! @retval char* \a inputfile pointer from \a argStruct \a args structure

const char * getArgInputfile(const argStruct * const args)
{
  return args->inputfile;
}

//! @brief Return \a outputfile member from \a argStruct structure
//!
//! @param[in] *args pointer to an argStruct structure
//! @retval char* \a outputfile pointer from \a argStruct \a args structure

const char * getArgOutputfile(const argStruct * const args)
{
  return args->outputfile;
}

//! @brief Return \a outputtype member from \a argStruct structure
//!
//! @param[in] *args pointer to an argStruct structure
//! @retval char \a outputtype from \a argStruct \a args structure

const char getArgOutputtype(const argStruct * const args)
{
  return args->outputtype;
}

//! @brief Return \a GUIOn member from \a argStruct structure
//!
//! @param[in] *args pointer to an argStruct structure
//! @retval char \a GUIOn from \a argStruct \a args structure

const char getArgGUIOn(const argStruct * const args)
{
  return args->GUIOn;
}

//! @brief Return \a NCOn member from \a argStruct structure
//!
//! @param[in] *args pointer to an argStruct structure
//! @retval char \a NCOn from \a argStruct \a args structure

const char getArgNCOn(const argStruct * const args)
{
  return args->NCOn;
}

//! @brief Return \a showVersion member from \a argStruct structure
//!
//! @param[in] *args pointer to an argStruct structure
//! @retval char \a showVersion from \a argStruct \a args structure

const char getArgVersion(const argStruct * const args)
{
  return args->showVersion;
}

//! @brief Return \a help member from \a argStruct structure
//!
//! @param[in] *args pointer to an argStruct structure
//! @retval char \a help from \a argStruct \a args structure

const char getArgHelp(const argStruct * const args)
{
  return args->help;
}

//! @brief Parse commandline arguments into an \a argStruct structure
//!
//! This function sets appropriate members of \a argStruct structure
//! based on arguments passed from a command line.
//! @param[in] argc pointer to an argument count
//! @param[in] *argv pointer to an argument vector
//! @param[out] *args pointer to an argStruct structure

void parseArgs(int argc, char *argv[], argStruct * const args)
{
  int opt;
  while ((opt = getopt(argc, argv, "d:i:o:ct:gnvh")) != -1) {
    switch (opt) {
    case 'd':
      args->dictfile = optarg;
      break;
    case 'i':
      args->inputfile = optarg;
      break;
    case 'o':
      args->outputfile = optarg;
      break;
    case 't':
      if(!strncmp(optarg, "audio", 5))
        args->outputtype = ARGAUDIO;
      else
        args->outputtype = ARGTEXT;
      break;
    case 'g':
      args->GUIOn = 1;
      break;
    case 'n':
      args->NCOn = 1;
      break;
    case 'v':
      args->showVersion = 1;
      break;
    case 'h':
    default: /* '?' */
      args->help = 1;
      break;
    }
  }
}
