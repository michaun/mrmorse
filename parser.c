/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "dictionary.h"
#include "textconv.h"
#include "toolkit.h"
#include "parser.h"

//! @file
//! @brief Contains functions for parsing \a Dictionary structures

//! @brief Load \a Sictionary from file \a filename
//!
//! Opens a file for reading and parses its contents
//! Expected syntax is "character[space]code"
//! Function first cleans the dictionary using cleanDictionary() function
//! Then reads and parses characters into the dictionary.
//! It is supposed to read thet whole file skipping malformed entries.\n
//! At the moment the logic is pretty basic so there is not much error-recovery
//! to be had.
//! @param[in] filename name of a file from which to load a \a Dictionary
//! @param[out] *dict Pointer to \a Dictionary structure to fill
//! @retval -1 if something went wrong
//! @retval 0 on success

int loadDictionaryFromFile(const char * filename, Dictionary * dict)
{
  FILE * fp;
  int32_t c;
  int32_t character;
  char error;
  unsigned int codeLen = 8;
  char * code;

  if(dict == NULL) return -1;
  if(filename == NULL) return -1;

  cleanDictionary(dict); // start fresh

  if (NULL == (fp = fopen(filename, "r"))) {
    return -1;
  }

  code = calloc(codeLen, sizeof(char));
  if(!code) return -1;
  
  while(EOF != (c = readNextUTF8(fp, NULL))) {
    character = c;
    memset(code, '\0', codeLen);
    error = 0;
    if((c = readNextUTF8(fp, NULL)) != ' ') error = 1; // malformed - but try to save as much dictionary as possible - continue;
    while('\n' != (c = fgetc(fp))) {
      if(c == EOF) break;
      if(c == ' ' || c == '\t') continue;
      if(strlen(code) >= codeLen-1) {
        codeLen *= 2;
        code = realloc(code, codeLen * sizeof(char));
      }
      if(!code) { fclose(fp); return -1; } // code may be NULL after resize
      code[strlen(code)] = c;
    }
    code[codeLen-1] = '\0';
    if(!error)
      addCodeChar(dict, character, ditdotToBinary(code));
  }

  freeNullify((void**)&code);
  fclose(fp);
  return 0;
}
