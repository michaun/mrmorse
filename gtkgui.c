/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <gtk/gtk.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "dictionary.h"
#include "arghandler.h"
#include "parser.h"
#include "textconv.h"
#include "soundconv.h"
#include "toolkit.h"
#include "version.h"

#include "gtkgui.h"

//! @file
//! @brief Contains functions fo using GTK GUI

//! @brief Contains arguments for GTK Callbacks
//!
//! This structure contains a bunch of pointers packed
//! into one structure. GTK callbacks allow one void*
//! pointer. Therefore this structure collects everything
//! needed and serves as an argument to pass around.

struct _gtkArgStruct {
  Dictionary * dict; //!< pointer to a \a Dictionary
  argStruct * args; //!< pointer to an \a argStruct
  GtkWidget * inWidget; //!< pointer to \a inWidget
  GtkWidget * outWidget; //!< pointer to \a outWidget
  GtkWidget * parentWidget; //!< pointer to \a parentWidget
};

//! Typedef for a \a struct \a _gtkArgStruct
typedef struct _gtkArgStruct gtkArgStruct;

static char * mallocGetTextBuffer(GtkWidget * b, int * buffLen)
{
  char * inbuffer;
  char * workingbuff;
  GtkTextIter startpos, endpos;
  GtkTextBuffer * textbuffer;

  textbuffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(b));
  gtk_text_buffer_get_start_iter(textbuffer, &startpos);
  gtk_text_buffer_get_end_iter(textbuffer, &endpos);
  
  inbuffer = gtk_text_buffer_get_text(textbuffer, &startpos, &endpos, FALSE);
  if(buffLen) *buffLen = strlen(inbuffer)+1;
  workingbuff = calloc(strlen(inbuffer)+1, sizeof(char));
  if(!workingbuff) return NULL;
  strncpy(workingbuff, inbuffer, strlen(inbuffer));
  freeNullify((void**)&inbuffer);

  return workingbuff;
}

static int loadBufferContentFromFile(GtkTextBuffer * textbuffer, const char * inputfile)
{
  char * inbuffer;
  gtk_text_buffer_set_text(textbuffer, "", 0); // clear the buffer before loading
  inbuffer = mallocLoadBufferFromFile(inputfile);
  gtk_text_buffer_insert_at_cursor(textbuffer, inbuffer, -1);
  freeNullify((void**)&inbuffer);
  return 0;
}


static int loadBufferContentFromDictionary(GtkTextBuffer * textbuffer, Dictionary * dict)
{
  char * incode;
  int32_t inchar;
  DictChar * dc = mallocDictChar();
  gtk_text_buffer_set_text(textbuffer, "", 0); // clear the buffer before loading
  for(int i = 0; i < getLength(dict); i++) {
    inchar = getCharAt(dict, i);
    fillDictChar(dc, inchar);
    gtk_text_buffer_insert_at_cursor(textbuffer, getDictCharPtr(dc), -1);
    gtk_text_buffer_insert_at_cursor(textbuffer, " ", 1);
    incode = mallocBinaryToDitDot(getCodeAt(dict, i), NULL);
    gtk_text_buffer_insert_at_cursor(textbuffer, incode, -1);
    gtk_text_buffer_insert_at_cursor(textbuffer, "\n", 1);
    freeNullify((void**)&incode);
  }
  freeNullify((void**)&dc);
  return 0;
}


static void printAudio(GtkWidget *widget, gpointer data)
{
  if(data == NULL) return;
  gtkArgStruct * dp = (gtkArgStruct *)data;
  g_print ("Hello World %s\n", getArgInputfile(dp->args));
  //convert(dp->dict, dp->args->inputfile, AUDIO);

  char * workingbuff = mallocGetTextBuffer(dp->inWidget, NULL);
  
  convertSnd(dp->dict, workingbuff, NULL);
  
  freeNullify((void**)&workingbuff);
}

static gint allocShowAboutDialog(GtkWidget ** dialog, GtkWidget * parentWindow)
{
  gint ret;
  const char * authors[] = { "Michal Niezborala", NULL };
  *dialog = gtk_about_dialog_new();
  gtk_about_dialog_set_program_name(GTK_ABOUT_DIALOG(*dialog), NAME);
  gtk_about_dialog_set_copyright (GTK_ABOUT_DIALOG(*dialog), COPYRIGHT);
  gtk_about_dialog_set_comments (GTK_ABOUT_DIALOG(*dialog), DESCRIPTION);
  gtk_about_dialog_set_license_type (GTK_ABOUT_DIALOG(*dialog), GTK_LICENSE_GPL_3_0);
  gtk_about_dialog_set_authors (GTK_ABOUT_DIALOG(*dialog), authors);
  gtk_about_dialog_set_version (GTK_ABOUT_DIALOG(*dialog), VERSION);
  ret = gtk_dialog_run (GTK_DIALOG (*dialog));
  return ret;
}

static gint allocShowOpenDialog(GtkWidget ** dialog, char ** filename, GtkWidget * parentWindow)
{
  GtkFileChooser *chooser;
  GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
  gint ret;

  *dialog = gtk_file_chooser_dialog_new ("Open File",
                                         GTK_WINDOW(parentWindow),
                                         action,
                                         "_Cancel",
                                         GTK_RESPONSE_CANCEL,
                                         "_Open",
                                         GTK_RESPONSE_ACCEPT,
                                         NULL);

  chooser = GTK_FILE_CHOOSER (*dialog);
  ret = gtk_dialog_run (GTK_DIALOG (*dialog));
  *filename = gtk_file_chooser_get_filename (chooser);

  return ret;
}
  
static gint allocShowSaveDialog(GtkWidget ** dialog, char ** filename, GtkWidget * parentWindow)
{
  GtkFileChooser *chooser;
  GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
  gint ret;

  *dialog = gtk_file_chooser_dialog_new ("Save File",
                                         GTK_WINDOW(parentWindow),
                                         action,
                                         "_Cancel",
                                         GTK_RESPONSE_CANCEL,
                                         "_Save",
                                         GTK_RESPONSE_ACCEPT,
                                         NULL);
  chooser = GTK_FILE_CHOOSER (*dialog);
  gtk_file_chooser_set_do_overwrite_confirmation (chooser, TRUE);
  ret = gtk_dialog_run (GTK_DIALOG (*dialog));
  *filename = gtk_file_chooser_get_filename (chooser);

  return ret;
}

static void deallocDialog(GtkWidget * dialog, char * filename)
{
  g_free(filename);
  gtk_widget_destroy (dialog);
}

static void showAbout(GtkWidget *widget, gpointer data)
{
  GtkWidget * dialog;
  //gint ret;
  if(data == NULL) return;
  gtkArgStruct * dp = (gtkArgStruct *)data;
  
  allocShowAboutDialog(&dialog, dp->parentWidget);
  gtk_widget_destroy(dialog);
}

static void printAudioToFile(GtkWidget *widget, gpointer data)
{
  GtkWidget * dialog;
  char * filename;
  gint res;

  if(data == NULL) return;
  gtkArgStruct * dp = (gtkArgStruct *)data;
  
  res = allocShowSaveDialog(&dialog, &filename, dp->parentWidget);
  if (res == GTK_RESPONSE_ACCEPT) {
    g_print ("Hello World %s\n", getArgInputfile(dp->args));
    //convert(dp->dict, dp->args->inputfile, AUDIO);

    char * workingbuff = mallocGetTextBuffer(dp->inWidget, NULL);
    convertSnd(dp->dict, workingbuff, filename);
    freeNullify((void**)&workingbuff);
  }

  deallocDialog(dialog, filename);
}

static void internalPrintText(GtkWidget * inWidget, GtkWidget *outWidget, Dictionary * dict)
{
  char * workingbuff = mallocGetTextBuffer(inWidget, NULL);
  char * outbuffer;

  outbuffer = mallocConvertTxt(dict, workingbuff, NULL);
  
  g_print ("Hello World2 %s\n", outbuffer);
  gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(outWidget)), outbuffer, -1);
  
  freeNullify((void**)&outbuffer);
  freeNullify((void**)&workingbuff);
}  

static void printText(GtkWidget *widget, gpointer data)
{
  if(data == NULL) return;
  gtkArgStruct * dp = (gtkArgStruct *)data;
  g_print ("Hello World %s\n", getArgInputfile(dp->args));
  internalPrintText(dp->inWidget, dp->outWidget, dp->dict);
}

static void convertInPlace(GtkWidget *widget, gpointer data)
{
  if(data == NULL) return;
  gtkArgStruct * dp = (gtkArgStruct *)data;
  g_print ("Hello World %s\n", getArgInputfile(dp->args));
  internalPrintText(dp->inWidget, dp->inWidget, dp->dict);
}  

static void swapWindows(GtkWidget *widget, gpointer data)
{
  if(data == NULL) return;
  gtkArgStruct * dp = (gtkArgStruct *)data;
  g_print ("Hello World %s\n", getArgInputfile(dp->args));

  char * inbuff = mallocGetTextBuffer(dp->inWidget, NULL);
  char * outbuff = mallocGetTextBuffer(dp->outWidget, NULL);
  gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(dp->inWidget)), outbuff, -1);
  gtk_text_buffer_set_text(gtk_text_view_get_buffer(GTK_TEXT_VIEW(dp->outWidget)), inbuff, -1);
  
  freeNullify((void**)&inbuff);
  freeNullify((void**)&outbuff);
}  

static void printInputTextToFile(GtkWidget *widget, gpointer data)
{
  GtkWidget * dialog;
  char * filename;
  gint res;

  if(data == NULL) return;
  gtkArgStruct * dp = (gtkArgStruct *)data;

  res = allocShowSaveDialog(&dialog, &filename, dp->parentWidget);
  if (res == GTK_RESPONSE_ACCEPT) {
    char * workingbuff = mallocGetTextBuffer(dp->inWidget, NULL);

    saveBufferToFile(workingbuff, filename);
    freeNullify((void**)&workingbuff);
  }

  deallocDialog(dialog, filename);
}

static void printTextToFile(GtkWidget *widget, gpointer data)
{
  GtkWidget * dialog;
  char * filename;
  gint res;

  if(data == NULL) return;
  gtkArgStruct * dp = (gtkArgStruct *)data;

  res = allocShowSaveDialog(&dialog, &filename, dp->parentWidget);
  if (res == GTK_RESPONSE_ACCEPT) {
    char * workingbuff = mallocGetTextBuffer(dp->inWidget, NULL);
    char * outbuffer;

    outbuffer = mallocConvertTxt(dp->dict, workingbuff, NULL);

    saveBufferToFile(outbuffer, filename);
    
    freeNullify((void**)&outbuffer);
    freeNullify((void**)&workingbuff);
  }

  deallocDialog(dialog, filename);
}

static void openInputFile(GtkWidget *widget, gpointer data)
{
  GtkWidget * dialog;
  char * filename;
  gint res;

  if(data == NULL) return;
  gtkArgStruct * dp = (gtkArgStruct *)data;

  res = allocShowOpenDialog(&dialog, &filename, dp->parentWidget);

  if (res == GTK_RESPONSE_ACCEPT) {
    loadBufferContentFromFile(gtk_text_view_get_buffer(GTK_TEXT_VIEW(dp->inWidget)), filename);
  }
  
  deallocDialog(dialog, filename);
}

static void activate (GtkApplication *app, gpointer user_data)
{
  GtkWidget *window;
  GtkWidget *grid;
  GtkWidget *textbutton;
  GtkWidget *audiobutton;
  GtkWidget *swapbutton;
  GtkWidget *inplacebutton;
  GtkWidget *quitbutton;
  GtkWidget *outputtext;
  GtkWidget *inputtext;
  GtkWidget *dicttext;
  GtkWidget* scrolledinput;
  GtkWidget* scrolledoutput;
  GtkWidget* scrolleddict;
  GtkWidget* instructlabel;

  GtkWidget *menubar;
  GtkWidget *filemenu;
  GtkWidget *fileitem;
  GtkWidget *helpmenu;
  GtkWidget *helpitem;
  //filemenu items
  GtkWidget *openmenuitem;
  GtkWidget *saveinputmenuitem;
  GtkWidget *saveoutputmenuitem;
  GtkWidget *saveoutputmenu;
  //save output items
  GtkWidget *saveconvertedaudio;
  GtkWidget *saveconvertedtext;
  //help items
  GtkWidget *aboutitem;
  //
  gtkArgStruct * ps = (gtkArgStruct *) user_data;
  
  window = gtk_application_window_new (app);
  gtk_window_set_title (GTK_WINDOW (window), NAME);
  gtk_container_set_border_width (GTK_CONTAINER (window), 10);
  grid = gtk_grid_new ();
  gtk_container_add (GTK_CONTAINER (window), grid);

  ps->parentWidget = window; // set the parent

  //
  menubar = gtk_menu_bar_new();
  filemenu = gtk_menu_new();
  helpmenu = gtk_menu_new();
  saveoutputmenu = gtk_menu_new();

  fileitem = gtk_menu_item_new_with_label ("File");
  helpitem = gtk_menu_item_new_with_label ("Help");

  openmenuitem = gtk_menu_item_new_with_label("Open Input...");
  saveinputmenuitem = gtk_menu_item_new_with_label("Save Input As...");
  saveoutputmenuitem = gtk_menu_item_new_with_label("Save Output");

  saveconvertedtext = gtk_menu_item_new_with_label("As Text...");
  saveconvertedaudio = gtk_menu_item_new_with_label("As Audio...");

  aboutitem = gtk_menu_item_new_with_label("About");
  
  gtk_menu_shell_append(GTK_MENU_SHELL(menubar), fileitem);
  gtk_menu_shell_append(GTK_MENU_SHELL(menubar), helpitem);
  
  gtk_menu_shell_append (GTK_MENU_SHELL (filemenu), openmenuitem);
  gtk_menu_shell_append (GTK_MENU_SHELL (filemenu), saveinputmenuitem);
  gtk_menu_shell_append (GTK_MENU_SHELL (filemenu), saveoutputmenuitem);

  gtk_menu_shell_append (GTK_MENU_SHELL (saveoutputmenu), saveconvertedtext);
  gtk_menu_shell_append (GTK_MENU_SHELL (saveoutputmenu), saveconvertedaudio);

  gtk_menu_shell_append (GTK_MENU_SHELL (helpmenu), aboutitem);

  gtk_menu_item_set_submenu(GTK_MENU_ITEM(fileitem), filemenu);
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(saveoutputmenuitem), saveoutputmenu);
  gtk_menu_item_set_submenu(GTK_MENU_ITEM(helpitem), helpmenu);
  //

  instructlabel = gtk_label_new("For Morse Code to Text use '.' (dot), '-' (dash), '|' to separate words");
  
  inputtext = gtk_text_view_new ();
  scrolledinput = gtk_scrolled_window_new(NULL, NULL);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(inputtext), GTK_TEXT_WINDOW_LEFT, 1);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(inputtext), GTK_TEXT_WINDOW_RIGHT, 1);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(inputtext), GTK_TEXT_WINDOW_TOP, 1);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(inputtext), GTK_TEXT_WINDOW_BOTTOM, 1);
  gtk_text_view_set_accepts_tab (GTK_TEXT_VIEW(inputtext), FALSE);
  gtk_text_view_set_monospace (GTK_TEXT_VIEW(inputtext), TRUE);
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(inputtext), GTK_WRAP_WORD);
  gtk_widget_set_size_request(scrolledinput, 300, 300);
  gtk_container_add(GTK_CONTAINER(scrolledinput), inputtext);
  gtk_widget_set_vexpand(scrolledinput, TRUE);
  gtk_widget_set_hexpand(scrolledinput, TRUE);

  if(getArgInputfile(ps->args))
    loadBufferContentFromFile(gtk_text_view_get_buffer(GTK_TEXT_VIEW(inputtext)), getArgInputfile(ps->args));
  
  outputtext = gtk_text_view_new ();
  scrolledoutput = gtk_scrolled_window_new(NULL, NULL);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(outputtext), GTK_TEXT_WINDOW_LEFT, 1);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(outputtext), GTK_TEXT_WINDOW_RIGHT, 1);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(outputtext), GTK_TEXT_WINDOW_TOP, 1);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(outputtext), GTK_TEXT_WINDOW_BOTTOM, 1);
  gtk_text_view_set_accepts_tab (GTK_TEXT_VIEW(outputtext), FALSE);
  gtk_text_view_set_monospace (GTK_TEXT_VIEW(outputtext), TRUE);
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(outputtext), GTK_WRAP_WORD);
  gtk_widget_set_size_request(scrolledoutput, 300, 300);
  gtk_container_add(GTK_CONTAINER(scrolledoutput), outputtext);
  gtk_widget_set_vexpand(scrolledoutput, TRUE);
  gtk_widget_set_hexpand(scrolledoutput, TRUE);

  ps->inWidget = inputtext;
  ps->outWidget = outputtext;

  dicttext = gtk_text_view_new ();
  scrolleddict = gtk_scrolled_window_new(NULL, NULL);
  gtk_text_view_set_editable(GTK_TEXT_VIEW(dicttext), FALSE);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(dicttext), GTK_TEXT_WINDOW_LEFT, 1);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(dicttext), GTK_TEXT_WINDOW_RIGHT, 1);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(dicttext), GTK_TEXT_WINDOW_TOP, 1);
  gtk_text_view_set_border_window_size (GTK_TEXT_VIEW(dicttext), GTK_TEXT_WINDOW_BOTTOM, 1);
  gtk_text_view_set_accepts_tab (GTK_TEXT_VIEW(dicttext), FALSE);
  gtk_text_view_set_monospace (GTK_TEXT_VIEW(dicttext), TRUE);
  gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW(dicttext), GTK_WRAP_WORD);
  gtk_widget_set_size_request(scrolleddict, 80, 400);
  gtk_container_add(GTK_CONTAINER(scrolleddict), dicttext);
  gtk_widget_set_vexpand(scrolleddict, TRUE);
  
  if(getArgDictfile(ps->args))
    loadBufferContentFromDictionary(gtk_text_view_get_buffer(GTK_TEXT_VIEW(dicttext)), ps->dict);
  
  audiobutton = gtk_button_new_with_label ("play converted sound");
  g_signal_connect (audiobutton, "clicked", G_CALLBACK (printAudio), user_data);

  textbutton = gtk_button_new_with_label ("print converted text");
  g_signal_connect (textbutton, "clicked", G_CALLBACK (printText), user_data);

  swapbutton = gtk_button_new_with_label ("swap window buffers");
  g_signal_connect (swapbutton, "clicked", G_CALLBACK (swapWindows), user_data);

  inplacebutton = gtk_button_new_with_label ("convert in-place");
  g_signal_connect (inplacebutton, "clicked", G_CALLBACK (convertInPlace), user_data);

  quitbutton = gtk_button_new_with_label ("Quit");
  g_signal_connect_swapped (quitbutton, "clicked", G_CALLBACK (gtk_widget_destroy), window);

  g_signal_connect (saveconvertedaudio, "activate", G_CALLBACK (printAudioToFile), user_data);
  g_signal_connect (saveconvertedtext, "activate", G_CALLBACK (printTextToFile), user_data);
  g_signal_connect (saveinputmenuitem, "activate", G_CALLBACK (printInputTextToFile), user_data);
  g_signal_connect (openmenuitem, "activate", G_CALLBACK (openInputFile), user_data);
  g_signal_connect (aboutitem, "activate", G_CALLBACK (showAbout), user_data);

  //grid, child, left, top, width, height
  gtk_grid_attach (GTK_GRID (grid), menubar,        0, 0, 3, 1);
  gtk_grid_attach (GTK_GRID (grid), instructlabel,  0, 1, 3, 1);
  gtk_grid_attach (GTK_GRID (grid), scrolledinput,  0, 2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), scrolledoutput, 1, 2, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), scrolleddict,   2, 2, 1, 4);
  gtk_grid_attach (GTK_GRID (grid), audiobutton,    0, 3, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), textbutton,     1, 3, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), swapbutton,     0, 4, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), inplacebutton,  1, 4, 1, 1);
  gtk_grid_attach (GTK_GRID (grid), quitbutton,     0, 5, 2, 1);

  
  gtk_widget_show_all (window);
}

//! @brief Create and run GTK application
//!
//! @param[in] *args pointer to an argStruct structure (parsed command line args)
//! @return GTK application exit code

int createGtkWindow(argStruct * args)
{
  GtkApplication * app;
  int status;

  Dictionary * dict = mallocDictionary(40);
  // parse file into arrays
  loadDictionaryFromFile(getArgDictfile(args), dict);

  gtkArgStruct gtkargs = { .dict = dict, .args = args, .inWidget = NULL, .outWidget = NULL, .parentWidget = NULL };
  
  app = gtk_application_new ("mr.morse", G_APPLICATION_FLAGS_NONE); //org.gtk.example
  
  g_print("%s\n", getArgDictfile(gtkargs.args));

  g_signal_connect (app, "activate", G_CALLBACK (activate), &gtkargs);

  status = g_application_run (G_APPLICATION (app), 0, NULL);
  g_object_unref (app);

  freeDictionary(&dict);

  return status;
}
