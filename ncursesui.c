/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <ncurses.h>
#include <menu.h>
#include <form.h>
#include <string.h>
#include <locale.h>
#include <assert.h>

#include "arghandler.h"
#include "soundconv.h"
#include "textconv.h"
#include "dictionary.h"
#include "parser.h"
#include "toolkit.h"
#include "version.h"

#include "ncursesui.h"

//! @file
//! @brief NCurses UI

//! Macro define for getting size of an array in elements
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))

//! Menu width define
#define MWIDTH 26

//! @brief enumeration listing special keys that may appear in the UI

typedef enum _SpecialKey {
  SPECIAL_F1,
  SPECIAL_F2,
  SPECIAL_F3,
  SPECIAL_F4,
  SPECIAL_F5,
  SPECIAL_F6,
  SPECIAL_F10,
  SPECIAL_UP,
  SPECIAL_DOWN,
  SPECIAL_LEFT,
  SPECIAL_RIGHT,
  SPECIAL_DEL,
  SPECIAL_UNKNOWN,
} SpecialKey;

static void cutEndSpaces(char * ptr);
static int loadBufferContentFromFile(WINDOW * win, const char * inputfile);
static int loadBufferContentFromDictionary(WINDOW * win, Dictionary * dict);
static int getWinLines(WINDOW * win);
static char * mallocGetTextBuffer(WINDOW *win, int * buffLen);
static char * mallocGetFilenameForm(char * description);
static void aboutPopup();
static void helpPopup();
static void cmdmenu(WINDOW *srcwin, WINDOW * dstwin, Dictionary * dict);
static void printAudio(WINDOW *winsrc, Dictionary * dict, char * outputfile);
static void printText(WINDOW *winsrc, WINDOW *windst, Dictionary * dict, char * outputfile);
static void swapWindows(WINDOW *winsrc, WINDOW *windst, Dictionary * dict);
static SpecialKey parseEscape(int32_t ch);

/////////////////////////////////////////////////////////////

//! @brief Ncurses UI start function
//!
//! @param[in] args argument structure (cmdline args) 
void ncursesUIStart(argStruct * args)
{
  WINDOW *srcborder, *dstborder, *dictborder;
  WINDOW *srcwin;
  WINDOW *dstwin;
  WINDOW *dictwin;
  WINDOW *menuwin;
  int32_t curx, cury, ch;
  char loopOff = 0;
  Dictionary * dict = mallocDictionary(40);
  DictChar * dc;
  SpecialKey sp;
  loadDictionaryFromFile(getArgDictfile(args), dict);

  setlocale(LC_ALL, "");

  initscr();

  noecho();
  //keypad(stdscr, TRUE);
  refresh();
  
  srcborder = newwin(LINES-1, COLS/3, 0, 0);
  box(srcborder, 0, 0);
  wrefresh(srcborder);

  srcwin = derwin(srcborder, LINES-3, (COLS/3)-2, 1, 1);
  loadBufferContentFromFile(srcwin, getArgInputfile(args));
  wrefresh(srcwin);

  dstborder = newwin(LINES-1, COLS/3, 0, COLS/3);
  box(dstborder, 0, 0);
  wrefresh(dstborder);

  dstwin = derwin(dstborder, LINES-3, (COLS/3)-2, 1, 1);
  wrefresh(dstwin);

  dictborder = newwin(LINES-1, COLS/3, 0, 2*COLS/3);
  box(dictborder, 0, 0);
  wrefresh(dictborder);

  dictwin = derwin(dictborder, LINES-3, (COLS/3)-2, 1, 1);
  loadBufferContentFromDictionary(dictwin, dict);
  wrefresh(dictwin);

  menuwin = newwin(1, COLS, LINES-1, 0);
  wprintw(menuwin, "[F1] Translate text | [F2] Play translation | [F3] Open/Close Menu | [F4] Morse help Popup | [F5] Conv in-place | [F6] Swap Windows | [F10] Quit | locale: %s", setlocale(LC_ALL, NULL));
  wrefresh(menuwin);

  refresh();
  wrefresh(srcwin);

  dc = mallocDictChar();
  while(!loopOff && (ch = readNextUTF8(stdin, NULL))) {
    fillDictChar(dc, ch);
    getyx(srcwin, cury, curx);

    switch(ch) {
    case 0x1b:
      sp = parseEscape(ch);
      switch(sp) {
      case SPECIAL_F1:
        printText(srcwin, dstwin, dict, NULL);
        break;
      case SPECIAL_F2:
        printAudio(srcwin, dict, NULL);
        break;
      case SPECIAL_F3:
        cmdmenu(srcwin, dstwin, dict);
        redrawwin(srcwin);
        redrawwin(dstwin);
        break;
      case SPECIAL_F4:
        helpPopup();
        redrawwin(srcwin);
        redrawwin(dstwin);
        break;
      case SPECIAL_F5:
        printText(srcwin, srcwin, dict, NULL);
        break;
      case SPECIAL_F6:
        swapWindows(srcwin, dstwin, dict);
        break;
      case SPECIAL_F10:
        loopOff = 1;
        break;
      case SPECIAL_UP:
        wmove(srcwin, --cury, curx);
        break;
      case SPECIAL_DOWN:
        wmove(srcwin, ++cury, curx);
        break;
      case SPECIAL_LEFT:
        wmove(srcwin, cury, --curx);
        break;
      case SPECIAL_RIGHT:
        wmove(srcwin, cury, ++curx);
        break;
      default:
        break;
      }
      break;
    case 0x7f: //KEY_BACKSPACE:
      if(curx) {
        mvwdelch(srcwin, cury, --curx);
      } else {
        char * workingbuff = calloc(COLS, sizeof(char));
        if(!workingbuff) break;
        mvwinnstr(srcwin, --cury, 0, workingbuff, COLS);
        cutEndSpaces(workingbuff);
        curx = strlen(workingbuff);
        wmove(srcwin, cury, curx);
        freeNullify((void**)&workingbuff);
      }
      wrefresh(dictwin);
      wrefresh(srcwin);
      break;

    case 0xd: //enter
      wprintw(srcwin, "\n");
      ++cury;
      curx = 0;
      break;

    case 0xffffffff:
      // resize gives that crap
      break;
    default:
      wprintw(srcwin, "%s", getDictCharPtr(dc));
      curx++;
      break;
    }
    wmove(srcwin, cury, curx);
    wrefresh(srcwin);
  }
  freeNullify((void**)&dc);

  delwin(srcborder);
  delwin(dstborder);
  delwin(dictborder);

  delwin(srcwin);
  delwin(dstwin);
  delwin(dictwin);

  endwin();      /* End curses mode      */
  freeDictionary(&dict);

  return;
}

/////////////////////////////////////////////////////////
static void cutEndSpaces(char * ptr)
{
  while(*ptr != '\0') ptr++; //reach the end
  ptr--; // back up;
  while(*ptr == ' ') ptr--;
  ptr++; // go one after
  *ptr = '\0';
  return;
}

static int loadBufferContentFromFile(WINDOW * win, const char * inputfile)
{
  char * inbuffer;
  werase(win);
  inbuffer = mallocLoadBufferFromFile(inputfile);
  waddstr(win, inbuffer);
  freeNullify((void**)&inbuffer);
  return 0;
}

static int loadBufferContentFromDictionary(WINDOW * win, Dictionary * dict)
{
  char * incode;
  int32_t inchar;
  char cury = 0, curx = 0, maxy = 0, maxx = 0;
  char biggestx = 0, colstart = 0;
  DictChar * dc = mallocDictChar();
  werase(win);
  getmaxyx(win, maxy, maxx);maxx=maxx;
  for(int i = 0; i < getLength(dict); i++) {
    getyx(win, cury, curx);
    wmove(win, cury, colstart);
    inchar = getCharAt(dict, i);
    fillDictChar(dc, inchar);
    waddstr(win, getDictCharPtr(dc));
    waddnstr(win, " ", 1);
    incode = mallocBinaryToDitDot(getCodeAt(dict, i), NULL);
    waddstr(win, incode);
    getyx(win, cury, curx);
    if(curx > biggestx) biggestx = curx;
    waddnstr(win, "\n", 1);
    if(cury == maxy - 1) { colstart = biggestx + 1; wmove(win, 0, colstart); }
    freeNullify((void**)&incode);
  }
  freeNullify((void**)&dc);
  return 0;
}

static int getWinLines(WINDOW * win)
{
  int maxx, maxy;
  int lines = 0;
  getmaxyx(win, maxy, maxx);
  char * outbuffer = calloc(maxx+1, sizeof(char));
  if(!outbuffer) return 0;
  for(int i = 0; i < maxy; i++) {
    memset(outbuffer, 0, maxx+1);
    mvwinnstr(win, i, 0, outbuffer, maxx);
    cutEndSpaces(outbuffer);
    if(strlen(outbuffer)) lines = i+1;
  }
  freeNullify((void**)&outbuffer);
  return lines;
}

static char * mallocGetFilenameForm(char * description)
{
  FORM *form;
  FIELD *fields[2];
  WINDOW *win_form;
  int ch;
  char loopOff = 0;
  char * workingbuff;
  char bufSize = 0;
  SpecialKey sp;
  
	win_form = newwin(5, 58, 8, 5);
	box(win_form, 0, 0);
	fields[0] = new_field(1, 55, 2, 0, 0, 0);
	fields[1] = NULL;
  
	set_field_opts(fields[0], O_VISIBLE | O_PUBLIC | O_EDIT | O_ACTIVE);
	set_field_back(fields[0], A_UNDERLINE);

	form = new_form(fields);
	set_form_win(form, win_form);
	set_form_sub(form, derwin(win_form, 3, 56, 1, 1));
	post_form(form);
	mvwprintw(win_form, 1, 1, description);
	wmove(win_form, 3, 1);
  
	refresh();
	wrefresh(win_form);

  while(!loopOff && (ch = readNextUTF8(stdin, NULL))) {
    switch (ch) {
    case 0x1b:
      sp = parseEscape(ch);
      switch(sp) {
      case SPECIAL_F3:
        loopOff = 1;
        break;
      case SPECIAL_LEFT:
        form_driver(form, REQ_PREV_CHAR);
        break;

      case SPECIAL_RIGHT:
        form_driver(form, REQ_NEXT_CHAR);
        break;
        
      case SPECIAL_DEL:
        form_driver(form, REQ_DEL_CHAR);
        break;
      default:
        break;
      }
      break;

    case 0x7f:
      form_driver(form, REQ_DEL_PREV);
      break;

    case 0xd:
      form_driver(form, REQ_NEXT_FIELD); // form needs it for active field to update
      bufSize = strlen(field_buffer(fields[0],0))+1;
      workingbuff = calloc(bufSize, sizeof(char));
      if(!workingbuff) break;
      strncpy(workingbuff, field_buffer(fields[0],0), bufSize);
      cutEndSpaces(workingbuff);
      loopOff = 1;
      break;

		default:
			form_driver(form, ch);
			break;
    }
    wrefresh(win_form);
  }

	unpost_form(form);
	free_form(form);
	free_field(fields[0]);
  wrefresh(win_form);
	delwin(win_form);
  return workingbuff;
}

static void helpPopup()
{
  WINDOW *helpwin;
  
  helpwin = newwin(6, strlen("Press 'q' to close this window")+3, 10, 3);
  box(helpwin, 0, 0);
  mvwprintw(helpwin, 1, 1, "For Morse Code to Text use");
  mvwprintw(helpwin, 2, 1, "'.' (dot), '|' (dash),");
  mvwprintw(helpwin, 3, 1, "'|' to separate words");
  mvwprintw(helpwin, 4, 1, "Press 'q' to close this window");
  wrefresh(helpwin);
  while(wgetch(helpwin) != 'q');
  delwin(helpwin);
}

static void aboutPopup()
{
  WINDOW *aboutwin;
  
  aboutwin = newwin(7, strlen(DESCRIPTION)+2, 10, 3);
  box(aboutwin, 0, 0);
  mvwprintw(aboutwin, 1, 1, DESCRIPTION);
  mvwprintw(aboutwin, 2, 1, VERSION);
  mvwprintw(aboutwin, 3, 1, COPYRIGHT);
  mvwprintw(aboutwin, 4, 1, "See COPYING for details");
  mvwprintw(aboutwin, 5, 1, "Press 'q' to close this window and go back");
  wrefresh(aboutwin);
  while(wgetch(aboutwin) != 'q');
  delwin(aboutwin);
}

static void cmdmenu(WINDOW *srcwin, WINDOW * dstwin, Dictionary * dict)
{
  ITEM ** menuItems;
  MENU *my_menu;
  WINDOW *my_menu_win;
  int n_choices, i;
  int32_t ch;
  char *choices[] = {
    "Open Input..",
    "Save Input..",
    "Save Output As Text..",
    "Save Output As Audio..",
    "About",
    (char *)NULL,
  };
  char * filenamebuff = NULL;
  char * winbuffer = NULL;
  char loopOff = 0;
  int ret = 0;
  SpecialKey sp;

  /* Create items */
  n_choices = ARRAY_SIZE(choices);
  menuItems = (ITEM **)calloc(n_choices, sizeof(ITEM *));
  if(!menuItems) return;
  for(i = 0; i < n_choices; ++i)
    menuItems[i] = new_item(choices[i], choices[i]);

  /* Crate menu */
  my_menu = new_menu((ITEM **)menuItems);

  /* Create the window to be associated with the menu */
  my_menu_win = newwin(9, MWIDTH, 4, 5);
  keypad(my_menu_win, TRUE);
     
  /* Set main window and sub window */
  set_menu_win(my_menu, my_menu_win);
  set_menu_sub(my_menu, derwin(my_menu_win, 5, MWIDTH-2, 3, 1));
  set_menu_format(my_menu, 5, 1);

  /* Print a border around the main window and print a title */
  box(my_menu_win, 0, 0);
  mvwprintw(my_menu_win, 1, 1, "Menu:");
  mvwaddch(my_menu_win, 2, 0, ACS_LTEE);
  mvwhline(my_menu_win, 2, 1, ACS_HLINE, MWIDTH-2);
  mvwaddch(my_menu_win, 2, MWIDTH-1, ACS_RTEE);
  refresh();
  
  /* Post the menu */
  post_menu(my_menu);
  wrefresh(my_menu_win);

  while(!loopOff && (ch = readNextUTF8(stdin, NULL))) {
      switch(ch) {
      case 0x1b:
        sp = parseEscape(ch);
        switch(sp) {
        case SPECIAL_F3:
          loopOff = 1;
          break;
        case SPECIAL_DOWN:
          menu_driver(my_menu, REQ_DOWN_ITEM);
          break;
        case SPECIAL_UP:
        menu_driver(my_menu, REQ_UP_ITEM);
          break;
          //case KEY_NPAGE:
          //menu_driver(my_menu, REQ_SCR_DPAGE);
          //break;
          //case KEY_PPAGE:
          //menu_driver(my_menu, REQ_SCR_UPAGE);
          //break;
        default:
          break;
        }
        break;
      case 0xd:
        switch(item_index(current_item(my_menu))) {
        case 0: // load input
          filenamebuff = mallocGetFilenameForm("Filename to open input from");
          if(-1 != loadBufferContentFromFile(srcwin, filenamebuff))
            loopOff = 1;
          break;

        case 1: // sve input
          filenamebuff = mallocGetFilenameForm("Filename to save input to");
          winbuffer = mallocGetTextBuffer(srcwin, NULL);
          ret = saveBufferToFile(winbuffer, filenamebuff);
          freeNullify((void**)&winbuffer);
          if(-1 != ret)
            loopOff = 1;
          break;

        case 2: //save output text
          filenamebuff = mallocGetFilenameForm("Filename to save output text to");
          winbuffer = mallocGetTextBuffer(dstwin, NULL);
          ret = saveBufferToFile(winbuffer, filenamebuff);
          freeNullify((void**)&winbuffer);
          if(-1 != ret)
            loopOff = 1;
          break;

        case 3: // save output audio
          filenamebuff = mallocGetFilenameForm("Filename to save output audio to");
          printAudio(srcwin, dict, filenamebuff);
          break;

        case 4: // avout
          aboutPopup();
          break;

        default:
          mvprintw(LINES-9, 0, "itemon: %s %d\n",
                   item_name(current_item(my_menu)),
                   item_index(current_item(my_menu)));
          break;
        }
        redrawwin(my_menu_win);
        redrawwin(srcwin);
        wrefresh(srcwin);
        break;
      }
      wrefresh(my_menu_win);
    }

  freeNullify((void**)&filenamebuff);
  
  /* Unpost and free all the memory taken up */
  unpost_menu(my_menu);
  free_menu(my_menu);
  for(i = 0; i < n_choices; ++i)
    free_item(menuItems[i]);
  // get rid of the window and refresh that step to show it.
  werase(my_menu_win);
  wrefresh(my_menu_win);
  // delete the window
  delwin(my_menu_win);
}

static char * mallocGetTextBuffer(WINDOW *win, int * buffLen)
{
  //int x, y;
  int maxx, maxy;
  int lines = 0;
  char * outbuffer;
  char * buffline;
  //getyx(winsrc, y, x);
  lines = getWinLines(win);
  getmaxyx(win, maxy, maxx);maxy=maxy;
  if(buffLen) *buffLen = (lines*(maxx+1))+1;
  outbuffer = calloc((lines*(maxx+1))+1, sizeof(char));
  if(!outbuffer) return NULL;
  buffline = calloc(maxx+1, sizeof(char));
  if(!buffline) { freeNullify((void**)&outbuffer); return NULL; }
  for(int i = 0; i < lines; i++) {
    mvwinnstr(win, i, 0, buffline, maxx);
    cutEndSpaces(buffline);
    strcat(outbuffer, buffline);
    strcat(outbuffer, "\n");
  }
  freeNullify((void**)&buffline);
  return outbuffer;
  //  wmove(winsrc, y, x); // bring back x, y
  //  wrefresh(winsrc);
}

static void printAudio(WINDOW * winsrc, Dictionary * dict, char * outfile)
{
  char * outbuffer;
  outbuffer = mallocGetTextBuffer(winsrc, NULL);
  convertSnd(dict, outbuffer, outfile);
  freeNullify((void**)&outbuffer);
}

static void printText(WINDOW *winsrc, WINDOW *windst, Dictionary * dict, char *outfile)
{
  char * outbuffer;
  char * workingbuff;
  
  workingbuff = mallocGetTextBuffer(winsrc, NULL);
  
  werase(windst);
  outbuffer = mallocConvertTxt(dict, workingbuff, NULL);
  if(outfile)
    saveBufferToFile(outbuffer, outfile);
  else
    wprintw(windst, "%s", outbuffer);
  wrefresh(windst);

  freeNullify((void**)&outbuffer);
  freeNullify((void**)&workingbuff);
}

static void swapWindows(WINDOW *winsrc, WINDOW *windst, Dictionary * dict)
{
  char * inbuff;
  char * outbuff;
  
  inbuff = mallocGetTextBuffer(winsrc, NULL);
  outbuff = mallocGetTextBuffer(windst, NULL);
  
  werase(winsrc);
  werase(windst);

  wprintw(winsrc, "%s", outbuff);
  wprintw(windst, "%s", inbuff);

  wrefresh(winsrc);
  wrefresh(windst);

  freeNullify((void**)&inbuff);
  freeNullify((void**)&outbuff);
}

static SpecialKey parseEscape(int32_t ch)
{
  assert(ch == 0x1b);
  SpecialKey s = SPECIAL_UNKNOWN;
  // escape sequence
  ch = readNextUTF8(stdin, NULL);
  switch(ch) {
  case 0x5b: //..
    ch = readNextUTF8(stdin, NULL);
    switch(ch) {
    case 0x31: //F1-8
      ch = readNextUTF8(stdin, NULL);
      switch(ch) {
      case 0x31: //..1
        s = SPECIAL_F1;
        break;
      case 0x32:
        s = SPECIAL_F2;
        break;
      case 0x33:
        s = SPECIAL_F3;
        break;
      case 0x34:
        s = SPECIAL_F4;
        break;
      case 0x35:
        s = SPECIAL_F5;
        break;
      case 0x37: // yup, this is seriously what's up, F6 is 0x37
        s = SPECIAL_F6;
        break;
      }
      ch = readNextUTF8(stdin, NULL); // read finishing tilde
      break;
    case 0x32: //9-12
      ch = readNextUTF8(stdin, NULL);
      switch(ch) {
      case 0x31: //..10
        s = SPECIAL_F10;
        break;
      }
      break;
    case 0x33: //DEL
      s = SPECIAL_DEL;
      break;
    case 0x41: //up arrow
      s = SPECIAL_UP;
      break;
    case 0x42: //down arrow
      s = SPECIAL_DOWN;
      break;
    case 0x43: //right arrow
      s = SPECIAL_RIGHT;
      break;
    case 0x44: //left arrow
      s = SPECIAL_LEFT;
      break;
    }
    break;
  }
  return s;
}
