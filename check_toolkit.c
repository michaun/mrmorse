/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <check.h>

#include <stdlib.h>

#include "toolkit.h"

START_TEST(testStrlenUTF81)
{
  int output;
  output = strlenUTF8((unsigned char*)"al a\n\noso\ni");
  ck_assert_int_eq(11, output);
} END_TEST

START_TEST(testFreeNullify1)
{
  char * c = calloc(20, sizeof(char));
  freeNullify((void**)&c);  
  ck_assert_ptr_null(c);
} END_TEST

START_TEST(testFreeNullify2)
{
  char * c = NULL;
  freeNullify((void**)&c);
  ck_assert_ptr_null(c);
} END_TEST

Suite* ToolkitTestSuite() {
  Suite* suite;
  TCase *tc;
  suite = suite_create("Toolkit Tests");
  tc = tcase_create("Core");
  tcase_add_test(tc, testStrlenUTF81);
  tcase_add_test(tc, testFreeNullify1);
  tcase_add_test(tc, testFreeNullify2);
  suite_add_tcase(suite, tc);

  return suite;
}
