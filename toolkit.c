/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "toolkit.h"

//! @file
//! @brief Contains helper functions not attached to any specific module

//! @brief Saves \a buffer to a File given as a \a filename
//!
//! Function opens a \a filename file prints buffer into the file
//! and then closes the file.
//! @param[in] buffer The buffer to be written to a file
//! @param[in] filename The name of the file that will be written to
//! @return -1 if the filename couldn't be opened
//! @return 0 on success

int saveBufferToFile(const char * buffer, const char * filename)
{
  FILE * outfile;
  
  outfile = fopen(filename, "w");
  if(!outfile) return -1;
  fprintf(outfile, "%s", buffer);
  fclose(outfile);
  return 0;
}

//! @brief Allocates a buffer and fills it with \a filename's contents
//!
//! Function internaly uses a \a mallocLoadBufferFromFileHandle() function
//! to allocate a buffer, file handler used is created using \a filename.
//! @param[in] filename The name of the file that will be read from
//! @return a malloc'ed buffer contating \a filename's content
//! @warning The returned buffer has to be freed

char * mallocLoadBufferFromFile(const char * filename)
{
  FILE * fp;
  char * inbuffer;
  if (NULL == (fp = fopen(filename, "r"))) {
    return NULL;
  }
  inbuffer = mallocLoadBufferFromFileHandle(fp);
  fclose(fp);
  return inbuffer;
}

//! @brief Allocates a \a buffer and fills it with file content pointed by \a *fp
//!
//! Function allocates some initial buffer and reads file contents into that buffer
//! The buffer is resized if necessary and ended with a '\0' character.
//! @param[in] *fp FILE handle to be read from
//! @return a malloc'ed buffer contating file content pointed by \a fp
//! @note file handle has to be open and is neither closed nor rewound by his function
//! @warning The returned buffer has to be freed

char * mallocLoadBufferFromFileHandle(FILE * fp)
{
  char c;           //< character read from file
  unsigned int bufSize = 64; //< total buffer size
  unsigned int bufLen =  0;  //< current buffer length
  if(!fp) return NULL;
  char * inbuffer = calloc(bufSize, sizeof(char)); //< pointer to buffer to be returned
  if(!inbuffer) return NULL;
  while((c = fgetc(fp)) != EOF) {
    bufLen = strlen(inbuffer);
    inbuffer[bufLen] = c;
    if(bufLen >= bufSize) {
      bufSize *= 2;
      inbuffer = realloc(inbuffer, bufSize * sizeof(char));
      if(!inbuffer) return NULL;
    }
  }
  
  inbuffer[strlen(inbuffer)] = '\0';
  return inbuffer;
}

//! @brief Read next UTF8 character from an \a fp FILE, if given, return \a countBytes num of bytes read
//!
//! @param[in] *fp FILE handle to be read from
//! @param[out] *countBytes filled with number of bytes read if not NULL
//! @retval int32_t an ordered 4byte integer (MSB to LSB)

int32_t readNextUTF8(FILE * fp, int * countBytes)
{
  unsigned int c = 0;
  unsigned char letter[4] = {0};
  int32_t ret;
  int count = 0;

  if(!fp) return 0;
  if(EOF == (c = fgetc(fp))) return c;
  if(c < 0xc0) {
    letter[0] = c; letter[1] = '\0';
    ret = letter[0];
    count = 1;
  } else if(c < 0xe0) {
    letter[0] = c; letter[1] = fgetc(fp); letter[2] = '\0';
    ret = (letter[0]<<8)|(letter[1]);
    count = 2;
  } else if(c < 0xf0) {
    letter[0] = c; letter[1] = fgetc(fp); letter[2] = fgetc(fp); letter[3] = '\0';
    ret = (letter[0]<<16)|(letter[1]<<8)|(letter[2]);
    count = 3;
  } else if(c > 0xf0) {
    letter[0] = c; letter[1] = fgetc(fp); letter[2] = fgetc(fp); letter[3] = fgetc(fp);
    ret = (letter[0]<<24)|(letter[1]<<16)|(letter[2]<<8)|(letter[3]);
    count = 4;
  }
  if(countBytes)
    *countBytes = count;
  return ret;
}

//! @brief Read next UTF8 character from a \a buffer, if given, return \a countBytes num of bytes read
//!
//! @param[in] buffer buffer to read from
//! @param[out] *countBytes filled with number of bytes read if not NULL
//! @retval int32_t an ordered 4byte integer (MSB to LSB)

int32_t readNextUTF8Buf(const unsigned char * buffer, int * countBytes)
{
  unsigned int c = 0;
  unsigned char letter[4] = {0};
  int32_t ret;
  int count = 0;

  if(!buffer) return 0;
  if('\0' == (c = *buffer)) return c;
  if(c < 0xc0) {
    letter[0] = c; letter[1] = '\0';
    ret = letter[0];
    count = 1;
  } else if(c < 0xe0) {
    letter[0] = c; letter[1] = *(++buffer); letter[2] = '\0';
    ret = (letter[0]<<8)|(letter[1]);
    count = 2;
  } else if(c < 0xf0) {
    letter[0] = c; letter[1] = *(++buffer); letter[2] = *(++buffer); letter[3] = '\0';
    ret = (letter[0]<<16)|(letter[1]<<8)|(letter[2]);
    count = 3;
  } else if(c > 0xf0) {
    letter[0] = c; letter[1] = *(++buffer); letter[2] = *(++buffer); letter[3] = *(++buffer);
    ret = (letter[0]<<24)|(letter[1]<<16)|(letter[2]<<8)|(letter[3]);
    count = 4;
  }
  if(countBytes)
    *countBytes = count;
  return ret;
}

//! @brief Return number of bytes for next UTF8 character in \a buffer
//!
//! @param[in] buffer buffer to get character from
//! @return Number of bytes the next character takes

int firstCharBytesUTF8(const unsigned char * buffer)
{
  unsigned int c = 0;

  if(!buffer) return 0;
  c = *buffer;
  if(c < 0xc0)       return 1;
  else if(c < 0xe0)  return 2;
  else if(c < 0xf0)  return 3;
  else if(c >= 0xf0) return 4;
  return 0;
}

//! @brief Return length (number of characters) of UTF8 encoded \a buffer
//!
//! @param[in] buffer UTF8 encoded buffer 
//! @return Length (in number of characters) of the \a buffer

int strlenUTF8(const unsigned char * buffer)
{
  int len = 0;
  int c;
  while(*buffer) {
    c = firstCharBytesUTF8(buffer);
    buffer += c;
    len++;
  }
  return len;
}

//! @brief Free \a *ptr pointer (check if not NULL) and set it to NULL
//!
//! @param[in] ptr pointer to pointer that is to be freed and set to NULL

void freeNullify(void ** ptr)
{
  if(!ptr && !*ptr) return;
  free(*ptr);
  *ptr = NULL;
}
  
