/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <check.h>
#include <stdlib.h>

#include "parser.h"
#include "dictionary.h"
#include "textconv.h"
#include "toolkit.h"

START_TEST(testBinaryToDitDot)
{
  char input = 0b101;
  char * actual;
  actual = mallocBinaryToDitDot(input, NULL);
  char * expected = ".-";
  ck_assert_str_eq(expected, actual);
  freeNullify((void**)&actual);
} END_TEST

START_TEST(testConvertTxtToken1)
{
  Dictionary * d = mallocDictionary(40);
  loadDictionaryFromFile("dict.txt", d);
  char * output8 = calloc(8, sizeof(char));
  test_convertTxtToken(d, ".-", &output8, 8, 1);
  ck_assert_str_eq("a", output8);
  freeDictionary(&d);
  freeNullify((void**)&output8);
} END_TEST

START_TEST(testConvertTxtToken2)
{
  Dictionary * d = mallocDictionary(40);
  loadDictionaryFromFile("dict.txt", d);
  char * tooSmall = calloc(1, sizeof(char));
  test_convertTxtToken(d, ".-", &tooSmall, 1, 1);
  ck_assert_str_eq("", tooSmall);
  freeDictionary(&d);
  freeNullify((void**)&tooSmall);
} END_TEST

START_TEST(testConvertTxtToken3)
{
  Dictionary * d = mallocDictionary(40);
  loadDictionaryFromFile("dict.txt", d);
  char * output8 = calloc(8, sizeof(char));
  test_convertTxtToken(d, "A", &output8, 8, 0);
  ck_assert_str_eq(".-", output8);
  freeDictionary(&d);
  freeNullify((void**)&output8);
} END_TEST

START_TEST(testConvertTxtToken4)
{
  Dictionary * d = mallocDictionary(40);
  loadDictionaryFromFile("dict.txt", d);
  char * tooSmall = calloc(1, sizeof(char));
  test_convertTxtToken(d, "A", &tooSmall, 1, 0);
  ck_assert_str_eq("", tooSmall);
  freeDictionary(&d);
  freeNullify((void**)&tooSmall);
} END_TEST

START_TEST(testConvertTxtToken5)
{
  Dictionary * d = mallocDictionary(40);
  loadDictionaryFromFile("dict.txt", d);
  char * output = calloc(8, sizeof(char));
  test_convertTxtToken(d, NULL, &output, 1, 0);
  ck_assert_str_eq("", output);
  freeDictionary(&d);
  freeNullify((void**)&output);
} END_TEST

START_TEST(testConvertTxtLine1)
{
  Dictionary * d = mallocDictionary(40);
  loadDictionaryFromFile("dict.txt", d);
  char * output = calloc(32, sizeof(char));
  test_convertTxtLine(d, ".- .- .-", &output, 32);
  ck_assert_str_eq("aaa", output);
  freeDictionary(&d);
  freeNullify((void**)&output);
} END_TEST

START_TEST(testConvertTxtLine2)
{
  Dictionary * d = mallocDictionary(40);
  loadDictionaryFromFile("dict.txt", d);
  char * output = calloc(32, sizeof(char));
  test_convertTxtLine(d, "", &output, 32);
  ck_assert_str_eq("", output);
  freeDictionary(&d);
  freeNullify((void**)&output);
} END_TEST

START_TEST(testConvertTxtLine3)
{
  Dictionary * d = mallocDictionary(40);
  loadDictionaryFromFile("dict.txt", d);
  char * output = calloc(32, sizeof(char));
  test_convertTxtLine(d, NULL, &output, 32);
  ck_assert_str_eq("", output);
  freeDictionary(&d);
  freeNullify((void**)&output);
} END_TEST

START_TEST(testConvertTxtLine4)
{
  Dictionary * d = mallocDictionary(40);
  loadDictionaryFromFile("dict.txt", d);
  char * output = calloc(32, sizeof(char));
  test_convertTxtLine(d, ".- .-.. | .-", &output, 32);
  ck_assert_str_eq("al a", output);
  freeDictionary(&d);
  freeNullify((void**)&output);
} END_TEST

START_TEST(testConvertTxt1)
{
  Dictionary * d = mallocDictionary(40);
  loadDictionaryFromFile("dict.txt", d);
  char * output;
  output = mallocConvertTxt(d, ".- .-.. | .-\n\n--- ... ---\n..", NULL);
  ck_assert_str_eq("al a\n\noso\ni", output);
  freeDictionary(&d);
  freeNullify((void**)&output);
} END_TEST

Suite* TextConvTestSuite() {
  Suite* suite;
  TCase *tc, *tcbtd, *tctoken, *tcline;
  suite = suite_create("TextConv Tests");
  //
  tcbtd = tcase_create("BinaryToDitDot");
  tcase_add_test(tcbtd, testBinaryToDitDot);
  suite_add_tcase(suite, tcbtd);

  tctoken = tcase_create("TC Token");
  tcase_add_test(tctoken, testConvertTxtToken1);
  tcase_add_test(tctoken, testConvertTxtToken2);
  tcase_add_test(tctoken, testConvertTxtToken3);
  tcase_add_test(tctoken, testConvertTxtToken4);
  tcase_add_test(tctoken, testConvertTxtToken5);
  suite_add_tcase(suite, tctoken);

  tcline = tcase_create("TC Line");
  tcase_add_test(tcline, testConvertTxtLine1);
  tcase_add_test(tcline, testConvertTxtLine2);
  tcase_add_test(tcline, testConvertTxtLine3);
  tcase_add_test(tcline, testConvertTxtLine4);
  suite_add_tcase(suite, tcline);

  tc = tcase_create("TextConv");
  tcase_add_test(tc, testConvertTxt1);
  suite_add_tcase(suite, tc);

  return suite;
}
