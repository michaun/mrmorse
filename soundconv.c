/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string.h>
#include <math.h>
#include <unistd.h>

#include <ao/ao.h>
#include <sndfile.h>

#include "soundconv.h"

#include "dictionary.h"
#include "toolkit.h"

//! @file
//! @brief Functions for sound conversion/output

static int playFreq(int fraction, char beep, ao_device * device, ao_sample_format format);
static int playFile(char *Filename, ao_device * device, ao_sample_format format);

static void binaryToDitDotSnd(int32_t code, ao_device * device, ao_sample_format format)
{
  uint32_t marker = 0x80000000;
  if(code == -1) return;
  while((code & marker) != marker) {
    marker >>= 1; // go lower
  }
  marker >>= 1; //omit code-marker
  while(marker != 0) {
    if(code & marker) playFreq(4, 1, device, format); //4
    else playFreq(8, 1, device, format); //8
    marker >>= 1;
  }
}

static void playLetter(const char * letter, ao_device * device, ao_sample_format format)
{
  char c;
  char filename[16] = {0};
  strncpy(filename, "res/", 4);
  if(*letter == -1 || *letter == ' ' || *letter == '\n') return;
  if(*letter == '|') {
    strncpy(filename+4, "space.wav", strlen("space.wav"));
  } else {
    c = *letter;
    if(c >= 'A' && c <= 'Z') c += 0x20;
    filename[4] = c;
    strncpy(filename+4+1, ".wav", 4);
  }
  playFile(filename, device, format);
}

static int initAoDevice(ao_device ** device, ao_sample_format * format, const char * outfilename)
{
  int defaultDriver;

  ao_initialize();
  if(outfilename && *outfilename)
    defaultDriver = ao_driver_id("wav");
  else
    defaultDriver = ao_default_driver_id();

  memset(format, 0, sizeof(*format));
  format->bits = 16;
  format->channels = 2;
  format->rate = 44100;
  format->byte_format = AO_FMT_NATIVE;

  if(outfilename && *outfilename)
    *device = ao_open_file(defaultDriver, outfilename, 1, format, NULL);
  else
    *device = ao_open_live(defaultDriver, format, NULL);
  if (!*device) return -1;
  return defaultDriver;
}

static int closeAoDevice(ao_device * device)
{
  ao_close(device);
  ao_shutdown();
  return 0;
}

static int playFreq(int fraction, char beep, ao_device * device, ao_sample_format format)
{
  char *buffer;
  int bufSize;
  int sample;
  float freq = 880.0;
  int i;

  //  initAoDevice(&device, &format);

  bufSize = (format.bits/8 * format.rate * format.channels)/fraction + 3000; // filled buffer + a bit of padding to get breaks in-between ditdots
  buffer = calloc(bufSize, sizeof(char));
  if(!buffer) return -1;
  if(beep == 1) {
    for (i = 0; i < format.rate/fraction; i++) {
      sample = (int)(0.75 * 32768.0 * sin(2 * M_PI * freq * ((float) i/format.rate)));

      buffer[4*i] = buffer[4*i+2] = sample & 0xff;
      buffer[4*i+1] = buffer[4*i+3] = (sample >> 8) & 0xff;
    }
  }
  
  ao_play(device, buffer, bufSize);

  freeNullify((void**)&buffer);
  //closeAoDevice(device);
  return 0;
}

static int playFile(char *filename, ao_device * device, ao_sample_format format)
{
  // this funtion is flawed, we basically ignore sample format read from file
  // because we already opened the device - this may backfire later.
  // but OH WELL, let's drink and enjoy now
  // just a quick note to self: i guess as long as files are wavs it plays out
  // quite well
  short * buffer;
  int bufSize;
  SF_INFO sfinfo;

  SNDFILE *file = sf_open(filename, SFM_READ, &sfinfo);

  memset(&format, 0, sizeof(format));

  switch (sfinfo.format & SF_FORMAT_SUBMASK) {
  case SF_FORMAT_PCM_16:
    format.bits = 16;
    break;
  case SF_FORMAT_PCM_24:
    format.bits = 24;
    break;
  case SF_FORMAT_PCM_32:
    format.bits = 32;
    break;
  case SF_FORMAT_PCM_S8:
    format.bits = 8;
    break;
  case SF_FORMAT_PCM_U8:
    format.bits = 8;
    break;
  default:
    format.bits = 16;
    break;
  }
  format.channels = sfinfo.channels;
  format.rate = sfinfo.samplerate;
  format.byte_format = AO_FMT_NATIVE;

  bufSize = (format.bits/8 * format.rate * format.channels);
  buffer = calloc(bufSize, sizeof(short));
  if(!buffer) return -1;
  while (1) {
    int read = sf_read_short(file, buffer, bufSize);
    if(read == 0) break; // end of file reached
    ao_play(device, (char *) buffer, (uint_32) (read * sizeof(short)));
  }

  freeNullify((void**)&buffer);
  sf_close(file);
  return 0;
}

static void convertSndToken(Dictionary * dict, const char * input, char isMorseCoded, ao_device * device, ao_sample_format format)
{
  int32_t c;
  DictChar * dc;
  int byteCount = 0;
  if(!(input && *input)) return;
  dc = mallocDictChar();
  if(isMorseCoded) {
    if(input[0] == '|') {
      playLetter("|", device, format);
    } else if(input[0] == '?') {
      // do absolutely nothing
    } else {
      fillDictChar(dc, getLetterByBinary(dict, ditdotToBinary(input)));
      playLetter(getDictCharPtr(dc), device, format);
    }
  } else {
    while(*input != '\0') {
      c = readNextUTF8Buf((unsigned char *)input, &byteCount);
      if(c >= 'A' && c <= 'Z') c += 0x20;
      binaryToDitDotSnd(getBinaryByLetter(dict, c), device, format);
      input+=byteCount;
      if(*input) playFreq(1, 0, device, format);//sleep(1); //next letter is commin
    }
    //sleep(1);
  }
}

//! @brief Using \a dict the function converts \a input into sound output that is either played or put to \a outfilename
//!
//! @param[in] dict Dictionary that provides translation
//! @param[in] input buffer to translate
//! @param[out] outfilename Name of the output file to which the sound will be written or NULL to output directly to speakers

void convertSnd(Dictionary * dict, const char * input, const char * outfilename)
{
  ao_device *device = NULL;
  ao_sample_format format;
  char * workingbuffBeginPtr = NULL;
  if(!input) return;
  if(0 == strlen(input)) return;
  
  char * workingbuff = calloc(strlen(input)+1, sizeof(char));
  if(!workingbuff) return;
  workingbuffBeginPtr = workingbuff;
  strncpy(workingbuff, input, strlen(input));

  char isMCoded = isMorseCoded(workingbuff);
  char * pch = strsep(&workingbuff, " \n");
  initAoDevice(&device, &format, outfilename);

  while(pch != NULL) {
    convertSndToken(dict, pch, isMCoded, device, format);
    pch = strsep(&workingbuff, " \n");
    if(pch && !isMCoded) { playFreq(1, 0, device, format); playFreq(1, 0, device, format); }//sleep(2); // space in-between whole words for MCoded playback
  }
  freeNullify((void**)&workingbuffBeginPtr);
  closeAoDevice(device);
}
