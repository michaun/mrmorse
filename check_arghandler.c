/*
    This file is a part of MrMorse
    Copyright (C) 2017 - 2018 Michal Niezborala <michaun _at_ protonmail [dot] com>

    MrMorse is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    MrMorse is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with MrMorse.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <check.h>

#include "arghandler.h"

START_TEST(testParseArgs1)
{
  char * argv[] = { "./mrmorse", "-d", "dict.txt" };
  int argc = 3;
  argStruct * args = mallocArgStruct();
  parseArgs(argc, argv, args);
  const char * actual = getArgDictfile(args);
  const char * expected = "dict.txt";
  ck_assert_str_eq(expected, actual);
  freeArgStruct(&args);
} END_TEST

Suite* ArgHandlerTestSuite() {
  Suite* suite;
  TCase *tc;
  suite = suite_create("ArgHandler Tests");
  //
  tc = tcase_create("ParseArgs");
  tcase_add_test(tc, testParseArgs1);
  suite_add_tcase(suite, tc);

  return suite;
}
